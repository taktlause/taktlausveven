from factory import post_generation
from factory.django import DjangoModelFactory

from .models import Quote


class QuoteFactory(DjangoModelFactory):
    class Meta:
        model = Quote

    quote = "Det er farleg å gå åleine! Ta dette."
    quoted_as = "Gamal mann"

    @post_generation
    def users(self, create, user_list):
        if not create or not user_list:
            return

        self.users.set(user_list)
