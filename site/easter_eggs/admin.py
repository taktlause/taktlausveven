from django.contrib.admin import ModelAdmin, site

from easter_eggs.models import Beverage


class BeverageAdmin(ModelAdmin):
    list_display = ("type", "addition", "created_by", "created")
    search_fields = ("addition", "created_by__username")
    list_filter = ("type",)


site.register(Beverage, BeverageAdmin)
