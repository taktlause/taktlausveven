from common.models import CreatedModifiedMixin
from django.db.models import CharField, TextChoices


class Beverage(CreatedModifiedMixin):
    class BeverageType(TextChoices):
        TEA = "TEA", "Te"
        COFFEE = "COFFEE", "Kaffi"

    type = CharField(
        "type",
        max_length=30,
        choices=BeverageType.choices,
        default=BeverageType.TEA,
    )

    addition = CharField("tilsetjing", max_length=255, blank=True)

    def __str__(self):
        return self.get_type_display

    class Meta:
        ordering = ["created"]
        get_latest_by = "created"
        verbose_name = "drikk"
        verbose_name_plural = "drikk"
