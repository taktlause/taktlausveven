from common.forms.widgets import AutocompleteSelect
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit
from django.forms import ModelForm

from easter_eggs.models import Beverage


class BeverageForm(ModelForm):
    helper = FormHelper()
    helper.add_input(Submit("submit", "Brygg"))

    class Meta:
        model = Beverage
        fields = ["type", "addition"]
        widgets = {"date": AutocompleteSelect}
