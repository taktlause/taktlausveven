import os

import PIL
from buttons.button_pdf_generator import button_pdf_generator
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import FileResponse, HttpResponse
from django.template.loader import render_to_string
from django.views.generic import CreateView, View

from easter_eggs.models import Beverage

from .forms import BeverageForm


class BeverageCreate(LoginRequiredMixin, CreateView):
    model = Beverage
    form_class = BeverageForm
    template_name = "common/forms/form.html"

    def get_context_data(self, *args, **kwargs):
        context_data = super().get_context_data(*args, **kwargs)
        context_data["form_title"] = "Brygg ein drikk"
        return context_data

    def form_valid(self, form):
        form.save()

        brew_type = form.cleaned_data["type"]
        match brew_type:
            case Beverage.BeverageType.COFFEE:
                response = HttpResponse(content="Eg er ei tekanne!")
                response.status_code = 418
            case Beverage.BeverageType.TEA:
                content = render_to_string("easter_eggs/tea.html")
                return HttpResponse(content)
            case _:
                response = HttpResponse(content="404 not found.")
        return response


class EasterEggButton(View):
    def get(self, request, *args, **kwargs):
        image_path = os.path.join(
            os.path.split(__file__)[0], "static", "easter_eggs", "easter_egg.png"
        )
        image = PIL.Image.open(image_path)
        pdf = button_pdf_generator([image], num_of_each=3)
        return FileResponse(
            pdf, content_type="application/pdf", filename="gratulerer.pdf"
        )
