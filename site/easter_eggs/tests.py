from accounts.factories import SuperUserFactory
from common.mixins import TestMixin
from django.test import TestCase
from django.urls import reverse

from easter_eggs.models import Beverage


class BeverageViewTestSuite(TestMixin, TestCase):
    def test_returns_418(self):
        """Returns 418 if you attempt to brew coffee."""
        self.client.force_login(SuperUserFactory())
        response = self.client.post(
            reverse("easter_eggs:BeverageCreate"),
            {"type": Beverage.BeverageType.COFFEE},
        )
        self.assertEqual(response.status_code, 418)


class EasterEggButtonTestSuite(TestMixin, TestCase):
    def get_url(self):
        return reverse("easter_eggs:EasterEggButton")

    def test_returns_pdf(self):
        response = self.client.get(
            self.get_url(),
        )
        self.assertEqual(response["content-type"], "application/pdf")
