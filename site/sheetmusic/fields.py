from django.forms import ModelMultipleChoiceField
from django.template.defaultfilters import date

from sheetmusic.models import Score


class ScoreMultipleChoiceField(ModelMultipleChoiceField):
    def label_from_instance(self, obj: Score) -> str:
        return f"{obj} ({obj.arrangement or date(obj.created, 'Y-m-d')})"
