# Generated by Django 4.2.2 on 2024-02-18 18:12

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("sheetmusic", "0010_score_composer"),
    ]

    operations = [
        migrations.AlterField(
            model_name="score",
            name="arrangement",
            field=models.CharField(
                blank=True,
                help_text="Kven som har skrive notane, t.d. Sivert O.",
                max_length=255,
                verbose_name="arrangement",
            ),
        ),
        migrations.AlterField(
            model_name="score",
            name="composer",
            field=models.CharField(
                blank=True,
                help_text="Kven som har komponert songen, t.d. Mozart.",
                max_length=255,
                verbose_name="komponist",
            ),
        ),
        migrations.AlterField(
            model_name="score",
            name="originally_from",
            field=models.CharField(
                blank=True,
                help_text="Kor songen er kjent frå, t.d. ABBA eller Frost.",
                max_length=255,
                verbose_name="opphaveleg ifrå",
            ),
        ),
        migrations.AlterField(
            model_name="score",
            name="transcribed_by",
            field=models.CharField(
                blank=True,
                help_text="Kven som har omformatert songen, t.d. kingsize overført til musecore for lettere leselighet.",
                max_length=255,
                verbose_name="transkribert av",
            ),
        ),
    ]
