from django.apps import AppConfig


class GigstatisticsConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "gigstatistics"
    verbose_name = "speleoppdragstatistikk"
