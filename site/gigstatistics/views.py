from django.views.generic import ListView

from gigstatistics.models import Gigstatistic


class GigstatisticList(ListView):
    model = Gigstatistic
    context_object_name = "gigstatistics"
    queryset = Gigstatistic.objects.order_by("name")
