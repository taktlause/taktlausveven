from common.models import CreatedModifiedMixin
from django.db.models import (
    CharField,
    ManyToManyField,
)
from events.models import Event


class Gig(CreatedModifiedMixin):
    name = CharField(
        "namn",
        max_length=255,
        unique=True,
    )

    events = ManyToManyField(
        verbose_name="hendingar",
        to=Event,
        blank=True,
    )

    class Meta:
        verbose_name = "speleoppdrag"
        verbose_name_plural = "speleoppdrag"

    def __str__(self):
        return f"{self.name}"
