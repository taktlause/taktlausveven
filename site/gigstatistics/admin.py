from django.contrib.admin import ModelAdmin, site

from .models import Gig


class GigAdmin(ModelAdmin):
    filter_horizontal = ("events",)
    readonly_fields = ("created", "created_by", "modified", "modified_by")


site.register(Gig, GigAdmin)
