from django import template
from django.db.models.fields.files import ImageFieldFile

from ..models import Thumbnail

register = template.Library()


def find_width_height(image: ImageFieldFile, max_height: int = None) -> (int, int):
    """
    Finds the correct width and height for an image.

    If `max_height` is `None` or less than or equal to the image's height,
    the image's width and height is returned.

    Else, width and height is scaled down
    to within `max_height` while preserving the image's aspect ratio.
    """
    if max_height is None or image.height <= max_height:
        return (image.width, image.height)

    aspect_ratio = image.width / image.height
    width = int(aspect_ratio * max_height)
    return (width, max_height)


@register.inclusion_tag("pictures/includes/image.html")
def image(
    image: ImageFieldFile,
    description: str,
    gallery: str = None,
    fallback_url: str = None,
    lightbox: bool = True,
    css_class: str = "",
    max_height: int = None,
):
    """
    Documentation for this template tag is available on the wiki:

    https://gitlab.com/taktlause/taktlausveven/-/wikis/Images
    """
    context = {
        "image_url": image.url if image else fallback_url,
        "description": description,
        "gallery": gallery,
        "lightbox": lightbox,
        "css_class": css_class,
    }
    if max_height:
        context["height"] = max_height
    if not image or not image.storage.exists(image.name):
        return context

    thumbnail = Thumbnail.get_thumbnail(image)
    if not thumbnail.thumbnail or not thumbnail.thumbnail.storage.exists(
        thumbnail.thumbnail.name
    ):
        return context

    width, height = find_width_height(thumbnail.thumbnail, max_height)
    context.update(
        {"thumbnail_url": thumbnail.thumbnail.url, "width": width, "height": height}
    )

    return context
