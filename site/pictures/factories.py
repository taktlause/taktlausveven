from factory import SubFactory, post_generation
from factory.django import DjangoModelFactory, ImageField

from .models import Gallery, Image


class GalleryFactory(DjangoModelFactory):
    class Meta:
        model = Gallery

    title = "Medaljegalla 2291"
    content = "Still going strong!"

    @post_generation
    def events(self, create, events_list):
        if not create or not events_list:
            return

        self.events.set(events_list)


class ImageFactory(DjangoModelFactory):
    class Meta:
        model = Image

    gallery = SubFactory(GalleryFactory)
    image = ImageField(color="blue")
    description = "A most famous image of the color blue."
