from datetime import date
from io import BytesIO

from autoslug.fields import AutoSlugField
from common.models import ArticleMixin, CreatedModifiedMixin
from django.core.files.base import ContentFile
from django.db.models import (
    CASCADE,
    CharField,
    DateField,
    FloatField,
    ForeignKey,
    ImageField,
    Index,
)
from django.db.models.fields.files import ImageFieldFile
from django.urls.base import reverse
from PIL import Image as PillowImage
from PIL.ImageOps import exif_transpose


class Gallery(ArticleMixin):
    """Model representing a gallery."""

    date = DateField("dato", default=date.today)
    date_to = DateField("til dato", null=True, blank=True)

    slug = AutoSlugField(
        verbose_name="lenkjenamn",
        populate_from="title",
        unique=True,
        editable=True,
    )

    class Meta:
        ordering = ["title"]
        verbose_name = "galleri"
        verbose_name_plural = "galleri"

    def get_absolute_url(self):
        return reverse("pictures:GalleryDetail", args=[self.slug])

    def images_latest(self):
        """Returns this gallery's images ordered by `created`, descending."""
        return self.images.order_by("-created")


class Image(CreatedModifiedMixin):
    """Model representing an image in a gallery."""

    gallery = ForeignKey(
        Gallery,
        related_name="images",
        on_delete=CASCADE,
        verbose_name="galleri",
    )
    image = ImageField("bilete", upload_to="pictures/")
    description = CharField("beskriving", max_length=1024, blank=True)
    order = FloatField(
        "rekkjefølgje",
        default=0,
        help_text=(
            "Definerer rekkjefølgja til biletet. "
            "Bilete med lik rekkjefølgje vert sortert etter tidspunkt for opplasting."
        ),
    )

    class Meta:
        ordering = ["order", "created"]
        verbose_name = "bilete"
        verbose_name_plural = "bilete"

    def __str__(self):
        return self.image.name

    def get_absolute_url(self):
        return self.image.url


class Thumbnail(CreatedModifiedMixin):
    thumbnail = ImageField("knøttbilete", upload_to="thumbnails/")
    identifier = CharField("identifikator", max_length=255, unique=True)

    class Meta:
        indexes = [Index(fields=["identifier"])]
        ordering = ["created"]
        verbose_name = "knøttbilete"
        verbose_name_plural = "knøttbilete"

    def __str__(self):
        return self.thumbnail.name

    @staticmethod
    def identifier_for_image(image: ImageFieldFile) -> str:
        return image.name

    @staticmethod
    def _create_thumbnail(image: ImageFieldFile) -> "Thumbnail":
        thumbnail, created = Thumbnail.objects.get_or_create(
            identifier=Thumbnail.identifier_for_image(image)
        )
        if not created:
            # A thumbnail was created for the same image concurrently
            return thumbnail

        # Pre-apply rotation specified by Exif data
        # since Exif data is stripped by `thumbnail()`
        pillow_image = exif_transpose(PillowImage.open(image))
        buffer = BytesIO()

        pillow_image.thumbnail((300, 300))
        pillow_image.save(buffer, "webp")
        buffer.seek(0)

        filename = f"{image.name}.webp"
        thumbnail.thumbnail.save(filename, ContentFile(buffer.read()))
        thumbnail.save()

        return thumbnail

    @staticmethod
    def get_thumbnail(image: ImageFieldFile) -> "Thumbnail":
        return Thumbnail.objects.filter(
            identifier=Thumbnail.identifier_for_image(image)
        ).first() or Thumbnail._create_thumbnail(image)
