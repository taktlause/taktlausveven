from http.client import UNPROCESSABLE_ENTITY

from common.breadcrumbs.breadcrumbs import Breadcrumb, BreadcrumbsMixin
from common.forms.views import DeleteViewCustom
from common.mixins import PermissionOrCreatedMixin
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db import transaction
from django.http import HttpRequest, HttpResponse, JsonResponse
from django.shortcuts import get_object_or_404, render
from django.urls import reverse, reverse_lazy
from django.utils.http import urlencode
from django.utils.timezone import now
from django.views.decorators.http import require_http_methods
from django.views.generic import CreateView, ListView, UpdateView
from render_block import render_block_to_string

from .forms import GalleryForm, ImageCreateForm, ImageUpdateForm
from .models import Gallery, Image


def nav_tabs_gallery_edit(gallery):
    """Returns tab data for editing a gallery."""
    return [
        {
            "url": reverse("pictures:GalleryUpdate", args=[gallery.slug]),
            "name": "Rediger galleri",
        },
        {
            "url": reverse("pictures:ImageCreate", args=[gallery.slug]),
            "name": "Last opp bilete",
        },
    ]


class GalleryList(LoginRequiredMixin, BreadcrumbsMixin, ListView):
    """View for viewing all galleries."""

    model = Gallery
    context_object_name = "galleries"

    def get_year(self) -> int:
        year = self.request.GET.get("year", "")
        return int(year) if year.isdecimal() else now().year

    def get_queryset(self):
        year = self.get_year()
        return (
            super()
            .get_queryset()
            .filter(date__year=year)
            .prefetch_related("images", "events")
            .exclude(images__isnull=True)
            .order_by("-date" if year == now().year else "date")
        )

    def get_context_data(self, **kwargs):
        kwargs["selected_year"] = self.get_year()

        years = set(
            Gallery.objects.order_by().values_list("date__year", flat=True).distinct()
        )
        years.add(now().year)
        kwargs["years_with_galleries"] = sorted(years, reverse=True)

        return super().get_context_data(**kwargs)

    @classmethod
    def get_breadcrumb(cls, gallery=None, **kwargs):
        if gallery:
            year = gallery.date.year
            return Breadcrumb(
                f'{reverse("pictures:GalleryList")}?{urlencode({"year": year})}',
                f"Fotoarkiv {year}",
            )
        else:
            return Breadcrumb(
                reverse("pictures:GalleryList"),
                "Fotoarkiv",
            )


class NewestImagesList(LoginRequiredMixin, BreadcrumbsMixin, ListView):
    model = Image
    context_object_name = "images"
    paginate_by = 50
    breadcrumb_parent = GalleryList

    def get_queryset(self):
        return (
            super()
            .get_queryset()
            .select_related("gallery")
            .prefetch_related("gallery__events")
            .order_by("-created")
        )


class GalleryDetail(LoginRequiredMixin, BreadcrumbsMixin, ListView):
    """View for viewing a single gallery."""

    model = Image
    context_object_name = "images"
    template_name = "pictures/gallery_detail.html"
    breadcrumb_parent = GalleryList

    gallery = None

    def get_gallery(self):
        if not self.gallery:
            self.gallery = get_object_or_404(Gallery, slug=self.kwargs["slug"])
        return self.gallery

    def get_queryset(self):
        return super().get_queryset().filter(gallery=self.get_gallery())

    def get_context_data(self, **kwargs):
        kwargs["gallery"] = self.get_gallery()
        return super().get_context_data(**kwargs)

    def get_breadcrumbs_kwargs(self):
        return {"gallery": self.get_gallery()}

    @classmethod
    def get_breadcrumb(cls, gallery, **kwargs):
        return Breadcrumb(gallery.get_absolute_url(), gallery)


class GalleryCreate(LoginRequiredMixin, BreadcrumbsMixin, CreateView):
    """View for creating a gallery."""

    model = Gallery
    form_class = GalleryForm
    template_name = "common/forms/form.html"
    breadcrumb_parent = GalleryList

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs["submit_text"] = "Lag galleri"
        return kwargs

    def get_success_url(self) -> str:
        return reverse("pictures:ImageCreate", args=[self.object.slug])


class ImageCreate(LoginRequiredMixin, BreadcrumbsMixin, CreateView):
    model = Image
    form_class = ImageCreateForm
    template_name = "pictures/image_form.html"
    breadcrumb_parent = GalleryDetail

    gallery = None

    def get_gallery(self):
        if not self.gallery:
            self.gallery = get_object_or_404(Gallery, slug=self.kwargs["slug"])
        return self.gallery

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs["gallery"] = self.get_gallery()
        return kwargs

    def get_context_data(self, **kwargs):
        kwargs["form_title"] = f'Last opp bilete til "{self.get_gallery()}"'
        kwargs["nav_tabs"] = nav_tabs_gallery_edit(self.get_gallery())
        return super().get_context_data(**kwargs)

    def form_valid(self, form):
        with transaction.atomic():
            # Update `modified` and `modified_by`
            self.get_gallery().save()
            super().form_valid(form)
            return JsonResponse({})

    def form_invalid(self, form):
        super().form_invalid(form)
        return JsonResponse({}, status=UNPROCESSABLE_ENTITY)

    def get_success_url(self) -> str:
        return reverse("pictures:GalleryUpdate", args=[self.get_gallery().slug])

    def get_breadcrumbs_kwargs(self):
        return {"gallery": self.get_gallery()}


class GalleryUpdate(LoginRequiredMixin, BreadcrumbsMixin, UpdateView):
    """View for updating a gallery and its images."""

    model = Gallery
    form_class = GalleryForm
    template_name_suffix = "_form_update"
    breadcrumb_parent = GalleryDetail

    def get_context_data(self, **kwargs):
        kwargs["nav_tabs"] = nav_tabs_gallery_edit(self.object)
        return super().get_context_data(**kwargs)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs["submit_text"] = "Lagre galleridetaljar"
        return kwargs

    def get_breadcrumbs_kwargs(self):
        return {"gallery": self.object}


@login_required
@require_http_methods(["POST"])
def image_update(request: HttpRequest, gallery_slug: str, image_pk: int):
    image = get_object_or_404(Image, pk=image_pk, gallery__slug=gallery_slug)
    form = ImageUpdateForm(request.POST, instance=image)

    if form.is_valid():
        image = form.save()
        messages.success(request, "Biletebeskriving lagra.")

    return HttpResponse(
        content=render_block_to_string(
            "pictures/image_forms.html",
            "image_form",
            context={"image": image},
            request=request,
        ),
    )


@login_required
@require_http_methods(["DELETE"])
def image_delete(request: HttpRequest, gallery_slug: str, image_pk: int):
    image = get_object_or_404(Image, pk=image_pk, gallery__slug=gallery_slug)
    image.delete()
    messages.success(request, "Bilete sletta.")
    return render(request, "common/includes/messages.html")


class GalleryDelete(PermissionOrCreatedMixin, BreadcrumbsMixin, DeleteViewCustom):
    model = Gallery
    success_url = reverse_lazy("pictures:GalleryList")
    permission_required = ("pictures.delete_gallery", "pictures.delete_image")
    breadcrumb_parent = GalleryDetail

    def get_breadcrumbs_kwargs(self):
        return {"gallery": self.object}
