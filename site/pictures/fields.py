from django.forms import ModelMultipleChoiceField
from django.template.defaultfilters import date

from .models import Gallery


class GalleryMultipleChoiceField(ModelMultipleChoiceField):
    def label_from_instance(self, obj: Gallery) -> str:
        return f'{obj} ({date(obj.date, "Y-m-d")})'
