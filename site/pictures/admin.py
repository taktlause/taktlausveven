from django.contrib import admin

from .models import Gallery, Image, Thumbnail


class ImageInline(admin.TabularInline):
    model = Image


class GalleryAdmin(admin.ModelAdmin):
    list_display = ("title", "created", "created_by")
    search_fields = ("title", "created_by__username")
    prepopulated_fields = {"slug": ("title",)}
    inlines = (ImageInline,)
    readonly_fields = ("created", "created_by", "modified", "modified_by")


class ImageAdmin(admin.ModelAdmin):
    list_display = ("__str__", "gallery")
    search_fields = ("image", "gallery__title")
    readonly_fields = ("created", "created_by", "modified", "modified_by")


class ThumbnailAdmin(admin.ModelAdmin):
    list_display = ("identifier", "thumbnail", "created")
    search_fields = ("identifier", "thumbnail")
    readonly_fields = ("created", "created_by", "modified", "modified_by")


admin.site.register(Gallery, GalleryAdmin)
admin.site.register(Image, ImageAdmin)
admin.site.register(Thumbnail, ThumbnailAdmin)
