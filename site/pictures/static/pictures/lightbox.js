function initializeLightBox(rootElement) {
    const imageElements = rootElement.querySelectorAll(
        "[data-toggle='lightbox']",
    );

    imageElements.forEach((image_element) =>
        image_element.addEventListener("click", () => {
            BigPicture({
                el: image_element,
                imgSrc: image_element.dataset.bp,
                gallery: image_element.dataset.gallery &&
                    document.querySelectorAll(
                        `[data-gallery="${image_element.dataset.gallery}"`,
                    ),
                loop: true,
            });
        })
    );
}

initializeLightBox(document);
htmx.onLoad((element) => initializeLightBox(element));
