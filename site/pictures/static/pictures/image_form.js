class Progress {
  constructor(totalValue) {
    this.container = document.querySelector("#progress-container");
    this.progressBar = document.querySelector(".progress-bar");
    this.errorList = document.querySelector("#error-list");

    this.container.classList.remove("d-none");
    this.errorList.textContent = "";

    this.totalValue = totalValue;
    this.currentValue = 0;

    this.progressBar.style.width = "0%";
    this.progressBar.setAttribute("aria-valuenow", this.currentValue);
    this.progressBar.setAttribute("aria-valuemin", 0);
    this.progressBar.setAttribute("aria-valuemax", this.totalValue);
  }

  get width() {
    const fraction = this.currentValue / this.totalValue;
    return `${Math.round(fraction * 100)}%`;
  }

  increment() {
    this.currentValue += 1;
    this.progressBar.setAttribute("aria-valuenow", this.width);
    this.progressBar.style.width = this.width;
    this.progressBar.textContent =
      `${this.currentValue}/${this.totalValue} - ${this.width}`;
  }

  get hasErrors() {
    return this.errorList.childElementCount > 0;
  }

  addError(errorMessage) {
    const listItem = document.createElement("li");
    listItem.textContent = errorMessage;
    this.errorList.appendChild(listItem);
  }
}

const formElement = document.querySelector("form");
const imageInput = document.querySelector("#id_image");
const submitButton = document.querySelector("#submit-id-submit");
formElement.addEventListener("submit", async (event) => {
  event.preventDefault();
  submitButton.disabled = true;
  const progress = new Progress(imageInput.files.length);

  for (const image of imageInput.files) {
    const formData = new FormData();
    formData.set("image", image);
    const response = await fetchWithCsrfFormData("POST", "", formData);

    progress.increment();
    if (!response.ok) {
      progress.addError(`Klarte ikkje å laste opp bilete "${image.name}".`);
    }
  }

  imageInput.value = null;
  submitButton.disabled = false;
  if (!progress.hasErrors) {
    window.location.href = formElement.dataset.next;
  }
});
