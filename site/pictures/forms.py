from common.forms.fields import MultipleImageField
from common.forms.widgets import AutocompleteSelectMultiple
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit
from django.forms import DateInput, ModelForm
from django.forms.models import inlineformset_factory
from events.fields import EventMultipleChoiceField
from events.models import Event

from .models import Gallery, Image


class GalleryForm(ModelForm):
    """Form for creating and editing galleries."""

    events = EventMultipleChoiceField(
        queryset=Event.objects.order_by("-start_time"),
        required=False,
        widget=AutocompleteSelectMultiple,
        label="Hendingar",
    )

    class Meta:
        model = Gallery
        fields = ["title", "date", "date_to", "content", "events"]
        widgets = {
            "date": DateInput(attrs={"type": "date"}),
            "date_to": DateInput(attrs={"type": "date"}),
        }

    def __init__(self, *args, submit_text, **kwargs):
        super().__init__(*args, **kwargs)

        self.helper = FormHelper()
        self.helper.add_input(Submit("submit", submit_text))

        if self.instance.id:
            self.initial["events"] = self.instance.events.all()

    def save(self):
        gallery = super().save()
        gallery.events.set(self.cleaned_data["events"])
        return gallery


class ImageCreateForm(ModelForm):
    """Form for creating images."""

    def __init__(self, gallery=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.add_input(Submit("submit", "Last opp bilete"))

        self.gallery = gallery
        if self.gallery:
            self.helper.attrs["data-next"] = self.gallery.get_absolute_url()

    def save(self, commit=True):
        """Saves the uploaded image. Ignores the `commit` parameter."""
        Image.objects.create(gallery=self.gallery, image=self.files.get("image"))

    class Meta:
        model = Image
        fields = ["image"]
        field_classes = {"image": MultipleImageField}


class ImageUpdateForm(ModelForm):
    """Form for updating an image."""

    class Meta:
        model = Image
        fields = ["description"]


class ImageFormsetHelper(FormHelper):
    template = "pictures/image_formset.html"


ImageFormSet = inlineformset_factory(
    Gallery,
    Image,
    form=ImageUpdateForm,
    extra=0,
)
ImageFormSet.helper = ImageFormsetHelper()
