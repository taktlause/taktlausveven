document.addEventListener("DOMContentLoaded", () => {
  new DataTable("#jacket_overview", {
    order: [[0, "asc"]],
    paging: false,
    language: {
      zeroRecords: "Ingen jakker funne",
      info: "Visar _TOTAL_ jakker",
      infoEmpty: "Ingen jakker funne",
      infoFiltered: "(filtrert fra totalt _MAX_ jakker)",
      search: "Søk:",
    },
  });
});
