document.addEventListener("DOMContentLoaded", () => {
  const dataTable = new DataTable("#table-balance", {
    order: [[1, "asc"]],
    paging: false,
    language: {
      zeroRecords: "Ingen brukarar funne",
      info: "Visar _TOTAL_ brukarar",
      infoEmpty: "Ingen brukarar funne",
      infoFiltered: "(filtrert fra totalt _MAX_ brukarar)",
      search: "Søk:",
    },
    columns: [{ orderable: false }, null, null, null, null, null],
  });

  const filterTableOnMembershipStatus = (membershipStatus) => {
    dataTable
      .column(2)
      .search(membershipStatus ? membershipStatus : "", true, false)
      .draw();
  };

  const membershipStatusSelecter = document.querySelector(
    "#select-membership-status"
  );

  filterTableOnMembershipStatus(membershipStatusSelecter.value);
  membershipStatusSelecter.addEventListener("input", (event) =>
    filterTableOnMembershipStatus(event.target.value)
  );
});
