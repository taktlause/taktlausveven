document.addEventListener("DOMContentLoaded", () => {
  const dataTable = new DataTable("#table-brews", {
    order: [[1, "desc"]],
    paging: false,
    language: {
      zeroRecords: "Ingen brygg funne",
      info: "Visar _TOTAL_ brygg",
      infoEmpty: "Ingen brygg funne",
      infoFiltered: "(filtrert fra totalt _MAX_ brygg)",
      search: "Søk:",
    },
    columns: [
      null,
      null,
      null,
      { orderable: false },
      null,
      null,
      null,
      null,
      null,
      null,
      { orderable: false },
    ],
  });

  const filterEmptyBrewsInTable = (showEmptyBrews) => {
    dataTable
      .column(5)
      .search(showEmptyBrews ? "" : "false", true, false)
      .draw();
  };

  const showEmptyBrewsCheckbox = document.querySelector("#show-empty-brews");

  filterEmptyBrewsInTable(showEmptyBrewsCheckbox.checked);
  showEmptyBrewsCheckbox.addEventListener("input", (event) =>
    filterEmptyBrewsInTable(event.target.checked)
  );
});
