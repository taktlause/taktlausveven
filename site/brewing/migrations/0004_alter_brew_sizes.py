# Generated by Django 4.2.2 on 2023-11-07 20:16

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("brewing", "0003_brewsize_alter_transaction_comment_brew_sizes_and_more"),
    ]

    operations = [
        migrations.AlterField(
            model_name="brew",
            name="sizes",
            field=models.ManyToManyField(
                blank=True,
                related_name="sizes",
                to="brewing.brewsize",
                verbose_name="storleikar",
            ),
        ),
    ]
