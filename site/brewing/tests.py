from http import HTTPStatus

from accounts.factories import SuperUserFactory, UserFactory
from common.constants.factories import ConstantFactory
from common.mixins import TestMixin
from django.db import IntegrityError
from django.templatetags.static import static
from django.test import TestCase
from django.urls import reverse

from .factories import BrewFactory, BrewSizeFactory, TransactionFactory
from .models import Transaction, TransactionType


class BrewTestSuite(TestMixin, TestCase):
    def test_surcharge(self):
        """Returns the value of the surcharge constant, cast to an integer."""
        ConstantFactory(name="Påslag på brygg i NOK", value="5")
        brew = BrewFactory()
        self.assertEqual(brew.surcharge(), 5)

    def test_price(self):
        """Should return the price of the brew, scaled to the inputted size, with the surcharge."""
        ConstantFactory(name="Påslag på brygg i NOK", value="2")
        brew = BrewFactory(price_per_liter=10)
        self.assertEqual(brew.price(BrewSizeFactory(size=0.33)), 6)
        self.assertEqual(brew.price(BrewSizeFactory(size=0.5)), 7)
        self.assertEqual(brew.price(BrewSizeFactory(size=2)), 22)

    def test_price_returns_none_if_price_per_liter_is_none(self):
        """Should return `None` if `price_per`liter` is not set."""
        brew = BrewFactory(price_per_liter=None, available_for_purchase=False)
        self.assertIsNone(brew.price(BrewSizeFactory()))

    def test_price_per_liter_must_be_positive(self):
        """Price per liter must be positive."""
        with self.assertRaises(IntegrityError):
            BrewFactory(price_per_liter=-5)

    def test_price_per_liter_required_to_be_available_for_purchase(self):
        """Price per liter should be required for the brew to be available for purchase."""
        with self.assertRaises(IntegrityError):
            BrewFactory(price_per_liter=None, available_for_purchase=True)

    def test_empty_brews_cannot_be_available_for_purchase(self):
        """Empty brews cannot be available for purchase."""
        with self.assertRaises(IntegrityError):
            BrewFactory(empty=True, available_for_purchase=True)

    def test_alcohol_by_volume(self):
        """Should return the ABV calculated from the OG and the FG."""
        brew = BrewFactory(OG=1.050, FG=1.010)
        self.assertAlmostEqual(brew.alcohol_by_volume(), 5.34, 2)
        brew = BrewFactory(OG=1.034, FG=1.002)
        self.assertAlmostEqual(brew.alcohol_by_volume(), 4.15, 2)
        brew = BrewFactory(OG=1.062, FG=0.992)
        self.assertAlmostEqual(brew.alcohol_by_volume(), 9.33, 2)

    def test_alcohol_by_volume_returns_none_if_missing_og_or_fg(self):
        """Should return `None` if either `OG` or `FG` is missing."""
        brew = BrewFactory(OG=None, FG=None)
        self.assertIsNone(brew.alcohol_by_volume())
        brew = BrewFactory(OG=1.050, FG=None)
        self.assertIsNone(brew.alcohol_by_volume())
        brew = BrewFactory(OG=None, FG=1.010)
        self.assertIsNone(brew.alcohol_by_volume())

    def test_default_logo(self):
        """`default_logo` should return a URL to the default logo."""
        self.assertEqual(
            BrewFactory().default_logo(), static("brewing/default-brew-logo.svg")
        )

    def test_empty_defaults_to_false(self):
        """A brew's emptiness should default to false."""
        self.assertFalse(BrewFactory().empty)

    def test_to_str(self):
        """`__str__` should be the brew's name."""
        brew = BrewFactory()
        self.assertEqual(str(brew), brew.name)


class TransactionTestSuite(TestMixin, TestCase):
    def test_to_str(self):
        """`__str__` should include user's name, the transaction type, and the amount."""
        transaction = TransactionFactory()
        self.assertIn(str(transaction.user), str(transaction))
        self.assertIn(transaction.get_type_display(), str(transaction))
        self.assertIn(str(transaction.amount), str(transaction))

    def test_balance(self):
        """Should return the sum of all transactions in the queryset, with deposits counted as negative."""
        user = UserFactory()
        for _ in range(3):
            TransactionFactory(user=user, amount=20, type=TransactionType.DEPOSIT)
            TransactionFactory(user=user, amount=10, type=TransactionType.PURCHASE)

        self.assertEqual(user.brewing_transactions.balance(), 30)

    def test_balance_returns_0_if_user_has_no_transactions(self):
        """Should return 0 if the user has no transactions."""
        user = UserFactory()
        self.assertIs(user.brewing_transactions.balance(), 0)

    def test_amount_must_be_positive(self):
        """The `amount` must be positive."""
        TransactionFactory(amount=20, type=TransactionType.DEPOSIT)
        TransactionFactory(amount=20, type=TransactionType.PURCHASE)
        with self.assertRaises(IntegrityError):
            TransactionFactory(amount=-20)


class BrewOverviewTestSuite(TestMixin, TestCase):
    def get_url(self):
        return reverse("brewing:BrewOverview")

    def test_requires_login(self):
        """Should require login."""
        self.assertLoginRequired(self.get_url())

    def test_only_shows_available_brews(self):
        """Should only show brews that are available."""
        available_brew = BrewFactory(available_for_purchase=True)
        unavailable_brew = BrewFactory(available_for_purchase=False)

        self.client.force_login(SuperUserFactory())
        response = self.client.get(self.get_url())
        self.assertIn(available_brew, response.context["available_brews"])
        self.assertNotIn(unavailable_brew, response.context["available_brews"])


class BrewListTestSuite(TestMixin, TestCase):
    def get_url(self):
        return reverse("brewing:BrewList")

    def test_requires_permission_for_creating_brews(self):
        """Should require permission for creating brews."""
        self.assertLoginRequired(self.get_url())


class BrewCreateTestSuite(TestMixin, TestCase):
    def get_url(self):
        return reverse("brewing:BrewCreate")

    def test_requires_permission_for_creating_brews(self):
        """Should require permission for creating brews."""
        self.assertPermissionRequired(self.get_url(), "brewing.add_brew")

    def test_redirects_to_brew_list(self):
        """Should redirect to the brew list."""
        self.client.force_login(SuperUserFactory())
        response = self.client.post(
            self.get_url(), {"name": "Two Towers", "price_per_liter": 9}
        )
        self.assertRedirects(response, reverse("brewing:BrewList"))


class BrewUpdateTestSuite(TestMixin, TestCase):
    def setUp(self) -> None:
        self.brew = BrewFactory()

    def get_url(self):
        return reverse("brewing:BrewUpdate", args=[self.brew.slug])

    def test_requires_permission_for_creating_brews(self):
        """Should require permission for changing brews."""
        self.assertPermissionRequired(self.get_url(), "brewing.change_brew")

    def test_redirects_to_brew_list(self):
        """Should redirect to the brew list."""
        self.client.force_login(SuperUserFactory())
        response = self.client.post(
            self.get_url(), {"name": "Two Towers", "price_per_liter": 9}
        )
        self.assertRedirects(response, reverse("brewing:BrewList"))


class BalanceListTestSuite(TestMixin, TestCase):
    def get_url(self):
        return reverse("brewing:BalanceList")

    def test_requires_permission_for_viewing_transactions(self):
        """Should require permission for viewing transactions."""
        self.assertPermissionRequired(self.get_url(), "brewing.view_transaction")

    def test_annotates_balance_purchased_deposited(self):
        """Should annotate users' balance, total purchased, and total deposited."""
        user = SuperUserFactory()
        TransactionFactory(amount=20, type=TransactionType.DEPOSIT, user=user)
        TransactionFactory(amount=20, type=TransactionType.DEPOSIT, user=user)
        TransactionFactory(amount=20, type=TransactionType.PURCHASE, user=user)

        self.client.force_login(user)
        response = self.client.get(self.get_url())

        user_in_response = response.context["users"].get(id=user.id)
        self.assertEqual(user_in_response.balance, 20)
        self.assertEqual(user_in_response.deposited, 40)
        self.assertEqual(user_in_response.purchased, 20)


class DepositCreateTestSuite(TestMixin, TestCase):
    def get_url(self):
        return reverse("brewing:DepositCreate")

    def test_requires_login(self):
        """Should require login."""
        self.assertLoginRequired(self.get_url())

    def test_sets_user_and_transaction_type(self):
        """
        Should set the transaction `user` to the logged-in user,
        and the transaction type to `DEPOSIT`.
        """
        user = UserFactory()
        self.client.force_login(user)
        self.client.post(self.get_url(), {"amount": 20})

        self.assertEqual(Transaction.objects.count(), 1)
        deposit = Transaction.objects.latest()
        self.assertEqual(deposit.user, user)
        self.assertEqual(deposit.type, TransactionType.DEPOSIT)

    def tests_ignores_changes_to_user_and_type(self):
        """Should ignore changes to the user and the transaction type."""
        logged_in_user = UserFactory()
        different_user = UserFactory()
        self.client.force_login(logged_in_user)
        self.client.post(
            self.get_url(),
            {"amount": 20, "user": different_user, "type": TransactionType.PURCHASE},
        )

        self.assertEqual(Transaction.objects.count(), 1)
        deposit = Transaction.objects.latest()
        self.assertEqual(deposit.user, logged_in_user)
        self.assertEqual(deposit.type, TransactionType.DEPOSIT)

    def test_rejects_amounts_0_or_smaller(self):
        """Should reject amounts that are 0 or smaller."""
        self.client.force_login(UserFactory())

        response = self.client.post(self.get_url(), {"amount": 0})
        self.assertFormError(
            response.context["form"],
            None,
            "Beløpet til ein transaksjon må vere større enn 0.",
        )

        response = self.client.post(self.get_url(), {"amount": -50})
        self.assertFormError(
            response.context["form"],
            None,
            "Beløpet til ein transaksjon må vere større enn 0.",
        )

    def test_increases_users_balance(self):
        """Should increase a user's balance."""
        user = UserFactory()
        self.assertEqual(user.brewing_transactions.balance(), 0)

        self.client.force_login(user)
        self.client.post(self.get_url(), {"amount": 20})
        self.assertEqual(user.brewing_transactions.balance(), 20)


class BrewPurchaseCreateTestSuite(TestMixin, TestCase):
    def get_url(self, brew=None, size=None):
        brew = brew or self.brew
        size = size or self.size
        return (
            f"{reverse('brewing:BrewPurchaseCreate', args=[brew.slug])}?size={size.pk}"
        )

    def setUp(self) -> None:
        self.size = BrewSizeFactory()
        self.brew = BrewFactory(sizes=[self.size])

    def test_requires_login(self):
        """Should require login."""
        self.assertLoginRequired(self.get_url())

    def test_sets_user_transaction_type_brew_and_size(self):
        """
        Should set the transaction `user` to the logged-in user,
        the transaction type to `PURCHASE`,
        the brew to the current brew,
        the size to the selected size,
        and the amount to the brew's price based on the size.
        """
        user = UserFactory()
        self.client.force_login(user)
        self.client.post(self.get_url())

        self.assertEqual(Transaction.objects.count(), 1)
        purchase = Transaction.objects.latest()
        self.assertEqual(purchase.user, user)
        self.assertEqual(purchase.type, TransactionType.PURCHASE)
        self.assertEqual(purchase.brew, self.brew)
        self.assertEqual(purchase.size, self.size)
        self.assertEqual(purchase.amount, self.brew.price(self.size))

    def tests_ignores_changes_to_user_type_amount_brew_and_size(self):
        """Should ignore changes to the user, transaction type, amount, brew, and size."""
        different_brew = BrewFactory()
        different_size = BrewSizeFactory(size=20)
        logged_in_user = UserFactory()
        different_user = UserFactory()
        self.client.force_login(logged_in_user)
        self.client.post(
            self.get_url(),
            {
                "amount": 20,
                "user": different_user,
                "type": TransactionType.DEPOSIT,
                "brew": different_brew,
                "size": different_size,
            },
        )

        self.assertEqual(Transaction.objects.count(), 1)
        purchase = Transaction.objects.latest()
        self.assertEqual(purchase.user, logged_in_user)
        self.assertEqual(purchase.type, TransactionType.PURCHASE)
        self.assertEqual(purchase.brew, self.brew)
        self.assertEqual(purchase.size, self.size)
        self.assertEqual(purchase.amount, self.brew.price(self.size))

    def test_decreases_users_balance(self):
        """Should decrease a user's balance."""
        user = UserFactory()
        self.assertEqual(user.brewing_transactions.balance(), 0)

        self.client.force_login(user)
        self.client.post(self.get_url())
        self.assertEqual(
            user.brewing_transactions.balance(), 0 - self.brew.price(self.size)
        )

    def test_returns_forbidden_for_unavailable_brews(self):
        """Should return `403 Forbidden` for unavailable brews."""
        self.client.force_login(SuperUserFactory())
        unavailable_brew = BrewFactory(available_for_purchase=False)

        response = self.client.get(self.get_url(unavailable_brew))
        self.assertEqual(response.status_code, HTTPStatus.FORBIDDEN)

    def test_returns_forbidden_for_unavailable_sizes(self):
        """Should return `403 Forbidden` for sizes that aren't available for the brew."""
        self.client.force_login(SuperUserFactory())
        unavailable_size = BrewSizeFactory(size=30)

        response = self.client.get(self.get_url(self.brew, unavailable_size))
        self.assertEqual(response.status_code, HTTPStatus.FORBIDDEN)
