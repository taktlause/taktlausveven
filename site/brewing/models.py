from math import ceil

from autoslug.fields import AutoSlugField
from common.constants.models import Constant
from common.markdown.fields import MarkdownField
from common.models import CreatedModifiedMixin
from django.conf import settings
from django.core.validators import MinValueValidator
from django.db.models import (
    CASCADE,
    SET_NULL,
    BooleanField,
    Case,
    CharField,
    CheckConstraint,
    F,
    FloatField,
    ForeignKey,
    ImageField,
    IntegerField,
    Manager,
    ManyToManyField,
    Model,
    Q,
    TextChoices,
    When,
)
from django.db.models.aggregates import Sum
from django.templatetags.static import static


class BrewSize(Model):
    size = FloatField(
        "storleik",
        help_text="Storleik i liter.",
        validators=[MinValueValidator(0)],
    )

    class Meta:
        ordering = ["size"]
        verbose_name = "bryggstorleik"
        verbose_name_plural = "bryggstorleikar"

    def __str__(self):
        return f"{self.size} L"


class Task(Model):
    task = CharField("oppgåve", max_length=255)

    class Meta:
        ordering = ["task"]
        verbose_name = "oppgåve"
        verbose_name_plural = "oppgåver"

    def __str__(self):
        return self.task


class Brew(CreatedModifiedMixin):
    name = CharField("namn", max_length=255, blank=True)
    description = MarkdownField("beskriving", blank=True)
    slug = AutoSlugField(
        verbose_name="lenkjenamn", populate_from="name", editable=True, unique=True
    )
    price_per_liter = IntegerField("literpris", blank=True, null=True)
    available_for_purchase = BooleanField("tilgjengeleg for anskaffing", default=False)
    empty = BooleanField(
        "tomt", default=False, help_text="Om brygget har vorte drukke opp."
    )
    OG = FloatField(
        "OG",
        blank=True,
        null=True,
        help_text="Original Gravity. Tettleiken av sukker i brygget før gjæring. Brukt for å berekne alkoholprosent.",
    )
    FG = FloatField(
        "FG",
        blank=True,
        null=True,
        help_text="Final Gravity. Tettleiken av sukker i brygget etter gjæring. Brukt for å berekne alkoholprosent.",
    )
    logo = ImageField("logo", upload_to="brewing/logos/", blank=True)

    sizes = ManyToManyField(
        BrewSize, related_name="sizes", verbose_name="storleikar", blank=True
    )

    def surcharge(self):
        """Returns the current surcharge for brews."""
        surcharge, _ = Constant.objects.get_or_create(
            name="Påslag på brygg i NOK", defaults={"value": "2"}
        )
        return int(surcharge.value)

    def price(self, size: BrewSize):
        if not self.price_per_liter:
            return None
        return ceil(self.price_per_liter * size.size) + self.surcharge()

    def alcohol_by_volume(self):
        """
        Calculates alcohol by volume (ABV) from OG and FG.
        Returns `None` if either OG or FG is missing.
        """
        if self.OG is None or self.FG is None:
            return None
        return (76.08 * (self.OG - self.FG) / (1.775 - self.OG)) * (self.FG / 0.794)

    def default_logo(self):
        """Returns a URL to the default logo."""
        return static("brewing/default-brew-logo.svg")

    def price_per_ABV(self):
        """
        Calculates the price of the brew per liter per ABV.
        """
        alcohol_by_volume = self.alcohol_by_volume()
        if (
            alcohol_by_volume is None
            or self.price_per_liter is None
            or alcohol_by_volume <= 0
        ):
            return None

        return self.price_per_liter / (alcohol_by_volume)

    class Meta:
        ordering = ["name"]
        get_latest_by = "created"
        verbose_name = "brygg"
        verbose_name_plural = "brygg"
        constraints = [
            CheckConstraint(
                check=(Q(price_per_liter__gt=0) | Q(price_per_liter=None)),
                name="brew_price_per_liter_must_be_positive",
                violation_error_message="Literprisen til eit brygg må vere større enn 0.",
            ),
            CheckConstraint(
                check=(~Q(price_per_liter=None, available_for_purchase=True)),
                name="brew_price_required_if_available_for_purchase",
                violation_error_message="Literpris er påkravd om brygget skal vere tilgjengeleg for anskaffing.",
            ),
            CheckConstraint(
                check=(~Q(empty=True, available_for_purchase=True)),
                name="empty_brews_cannot_be_available_for_purchase",
                violation_error_message="Tomme brygg kan ikkje vere tilgjengelege for anskaffing.",
            ),
        ]

    def __str__(self):
        return self.name


class TransactionType(TextChoices):
    PURCHASE = "PURCHASE", "Anskaffing"
    DEPOSIT = "DEPOSIT", "Innbetaling"


class TransactionManager(Manager):
    def balance(self):
        """
        Returns the balance of the manager's queryset, most commonly used to
        return a user's balance by calling `user.brewing_transactions.balance()`.
        """
        amount_sign_depending_on_type = Case(
            When(type=TransactionType.DEPOSIT, then=F("amount")),
            default=-F("amount"),
        )
        return (
            self.aggregate(balance=Sum(amount_sign_depending_on_type))["balance"] or 0
        )


class Transaction(CreatedModifiedMixin):
    objects = TransactionManager()

    user = ForeignKey(
        settings.AUTH_USER_MODEL,
        verbose_name="brukar",
        on_delete=CASCADE,
        related_name="brewing_transactions",
    )
    amount = IntegerField("beløp")
    brew = ForeignKey(
        Brew,
        on_delete=SET_NULL,
        related_name="transactions",
        verbose_name="brygg",
        null=True,
        blank=True,
    )
    size = ForeignKey(
        BrewSize,
        on_delete=SET_NULL,
        related_name="transactions",
        verbose_name="storleik",
        null=True,
        blank=True,
    )
    comment = CharField(
        "kommentar",
        max_length=255,
        blank=True,
        help_text="Kommentar til innbetalinga. Brukt i spesielle tilfelle og for å bevare eldre data.",
    )
    type = CharField(
        "type",
        max_length=30,
        choices=TransactionType.choices,
    )

    def __str__(self):
        return f"{self.user} – {self.get_type_display()} – {self.amount} NOK"

    class Meta:
        ordering = ["-created"]
        get_latest_by = "created"
        verbose_name = "transaksjon"
        verbose_name_plural = "transaksjonar"
        constraints = [
            CheckConstraint(
                check=Q(amount__gt=0),
                name="transaction_amount_must_be_positive",
                violation_error_message="Beløpet til ein transaksjon må vere større enn 0.",
            )
        ]
