from common.forms.widgets import AutocompleteSelectMultiple
from crispy_forms.helper import FormHelper
from crispy_forms.layout import HTML, Field, Fieldset, Layout, Submit
from django.forms import HiddenInput, ModelForm, NumberInput

from .models import Brew, BrewSize, Transaction, TransactionType


class BrewForm(ModelForm):
    helper = FormHelper()
    helper.layout = Layout(
        Fieldset(
            "Generelt",
            "name",
            "description",
            "logo",
        ),
        Fieldset(
            "Anskaffing",
            "price_per_liter",
            "sizes",
            "available_for_purchase",
            "empty",
        ),
        Fieldset(
            "Brygging",
            "OG",
            "FG",
        ),
        Submit("submit", "Lagre brygg"),
    )

    class Meta:
        model = Brew
        fields = [
            "name",
            "description",
            "price_per_liter",
            "sizes",
            "available_for_purchase",
            "empty",
            "logo",
            "OG",
            "FG",
        ]
        widgets = {"sizes": AutocompleteSelectMultiple}


class DepositForm(ModelForm):
    helper = FormHelper()
    helper.add_input(Submit("submit", "Legg inn pengar"))
    helper.layout = Layout(
        HTML(
            """
                {% load embeddable_text markdown %}
                {% get_embeddable_text "Innbetaling til bryggjekassa" as text %}
                {{ text | markdown }}
                """
        ),
        Field("amount", css_class="w-32"),
    )

    def __init__(self, user, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.initial["user"] = user
        self.initial["type"] = TransactionType.DEPOSIT
        self.fields["user"].disabled = True
        self.fields["type"].disabled = True

    class Meta:
        model = Transaction
        fields = ["amount", "user", "type"]
        widgets = {
            "amount": NumberInput(attrs={"min": 1}),
            "user": HiddenInput,
            "type": HiddenInput,
        }


class BrewPurchaseForm(ModelForm):
    helper = FormHelper()
    helper.add_input(Submit("submit", "Skaff"))

    def __init__(self, user, brew: Brew, size: BrewSize, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.initial["user"] = user
        self.initial["amount"] = brew.price(size)
        self.initial["brew"] = brew
        self.initial["size"] = size
        self.initial["type"] = TransactionType.PURCHASE
        self.fields["user"].disabled = True
        self.fields["amount"].disabled = True
        self.fields["brew"].disabled = True
        self.fields["size"].disabled = True
        self.fields["type"].disabled = True

    class Meta:
        model = Transaction
        fields = ["amount", "brew", "size", "user", "type"]
        widgets = {
            "amount": HiddenInput,
            "brew": HiddenInput,
            "size": HiddenInput,
            "user": HiddenInput,
            "type": HiddenInput,
        }
