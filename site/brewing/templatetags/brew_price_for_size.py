from django import template

from ..models import Brew, BrewSize

register = template.Library()


@register.simple_tag
def brew_price_for_size(brew: Brew, size: BrewSize):
    return brew.price(size)
