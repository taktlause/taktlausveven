from accounts.factories import UserFactory
from factory import SubFactory, post_generation
from factory.django import DjangoModelFactory

from .models import Brew, BrewSize, Task, Transaction, TransactionType


class BrewSizeFactory(DjangoModelFactory):
    class Meta:
        model = BrewSize
        django_get_or_create = ("size",)

    size = 0.5


class TaskFactory(DjangoModelFactory):
    class Meta:
        model = Task

    task = "Still til vevansvarleg."


class BrewFactory(DjangoModelFactory):
    class Meta:
        model = Brew

    name = "Gudbrandsdalsvatn"
    price_per_liter = 42
    available_for_purchase = True

    @post_generation
    def sizes(self, create, sizes_list):
        if not create or not sizes_list:
            return

        self.sizes.set(sizes_list)


class TransactionFactory(DjangoModelFactory):
    class Meta:
        model = Transaction

    user = SubFactory(UserFactory)
    amount = 20
    type = TransactionType.DEPOSIT
