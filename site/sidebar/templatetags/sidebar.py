from datetime import datetime, timedelta

from accounts.forms import ImageSharingConsentForm
from authentication.forms import LoginForm
from django import template
from django.db.models.query_utils import Q
from django.utils.timezone import make_aware
from events.models import Attendance, Event, EventAttendance
from feedback.models import FeedbackRecipient
from polls.models import Poll

register = template.Library()


@register.inclusion_tag("sidebar/sidebar.html")
def sidebar(user, request_path):
    poll_filter = poll_filter = (
        Q(public=True) if not (user and user.is_authenticated) else Q()
    )
    try:
        poll = Poll.objects.filter(poll_filter).latest()
    except Poll.DoesNotExist:
        poll = None

    brewing_balance = (
        user.brewing_transactions.balance() if user.is_authenticated else 0
    )
    today = make_aware(datetime.now())
    next_week = today + timedelta(days=7)
    attending_not_or_maybe = (
        Event.objects.filter(start_time__range=[today, next_week]).exclude(
            attendances__in=EventAttendance.objects.filter(
                person=user, status__in=[Attendance.ATTENDING, Attendance.ATTENDING_NOT]
            )
        )
        if user.is_authenticated
        else None
    )
    feedback_recipients = FeedbackRecipient.objects.all()

    return {
        "user": user,
        "request_path": request_path,
        "form_login": LoginForm(autofocus=False),
        "brewing_balance": brewing_balance,
        "form_image_sharing_consent": ImageSharingConsentForm(request_path),
        "poll": poll,
        "attending_not_or_maybe": attending_not_or_maybe,
        "feedback_recipients": feedback_recipients,
    }
