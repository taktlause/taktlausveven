from common.forms.widgets import AutocompleteSelect
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit
from django.forms import ModelForm, TextInput

from .models import Instrument, InstrumentType


class InstrumentForm(ModelForm):
    """Form for creating and/or updating instruments."""

    helper = FormHelper()
    helper.add_input(Submit("submit", "Lagre instrument"))

    class Meta:
        model = Instrument
        fields = [
            "type",
            "identifier",
            "location",
            "user",
            "serial_number",
            "state",
            "comment",
        ]
        widgets = {
            "type": AutocompleteSelect,
            "user": AutocompleteSelect,
            "location": AutocompleteSelect,
            "comment": TextInput,
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["type"].queryset = InstrumentType.objects.filter(
            group__is_actual_instrument=True
        )
