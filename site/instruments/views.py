from common.breadcrumbs.breadcrumbs import breadcrumb, breadcrumbs
from common.decorators import htmx
from common.forms.views import delete_view, simple_create_view, simple_update_view
from django.contrib.auth.decorators import login_required, permission_required
from django.shortcuts import get_object_or_404
from django.template.response import TemplateResponse
from django.urls import reverse, reverse_lazy
from watson import search

from .forms import InstrumentForm
from .models import Instrument, InstrumentGroup


@login_required
@htmx
@breadcrumb("Instrumentoversikt", reverse_lazy("instruments:InstrumentList"))
def instrument_list(request):
    instruments = Instrument.objects.prefetch_related("type__group", "location", "user")
    if request.GET.get("instrument_group"):
        instrument_group = get_object_or_404(
            InstrumentGroup.objects.all(), slug=request.GET.get("instrument_group")
        )
        instruments = instruments.filter(type__group=instrument_group)
    if request.GET.get("search"):
        instruments = search.filter(
            instruments, request.GET.get("search"), ranking=False
        )
    instruments = instruments.order_by("type__group", "type", "identifier")

    return TemplateResponse(
        request,
        "instruments/instrument_list.html",
        {
            "instruments": instruments,
            "instrument_groups": InstrumentGroup.objects.all(),
        },
    )


@login_required
@permission_required("instruments.add_instrument", raise_exception=True)
def instrument_create(request):
    return simple_create_view(
        request,
        InstrumentForm,
        success_url=reverse("instruments:InstrumentList"),
        context=breadcrumbs(instrument_list),
    )


@login_required
@permission_required("instruments.change_instrument", raise_exception=True)
def instrument_update(request, slug: str):
    instrument = get_object_or_404(Instrument.objects.all(), slug=slug)
    return simple_update_view(
        request,
        InstrumentForm,
        instrument,
        success_url=reverse("instruments:InstrumentList"),
        context=breadcrumbs(instrument_list),
    )


@login_required
@permission_required("instruments.delete_instrument", raise_exception=True)
def instrument_delete(request, slug: str):
    instrument = get_object_or_404(Instrument.objects.all(), slug=slug)
    return delete_view(
        request,
        instrument,
        reverse("instruments:InstrumentList"),
        context=breadcrumbs(instrument_list),
    )
