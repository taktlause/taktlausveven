from django.urls import path

from .views import (
    instrument_create,
    instrument_delete,
    instrument_list,
    instrument_update,
)

app_name = "instruments"

urlpatterns = [
    path("", instrument_list, name="InstrumentList"),
    path("nytt/", instrument_create, name="InstrumentCreate"),
    path("<slug:slug>/rediger/", instrument_update, name="InstrumentUpdate"),
    path("<slug:slug>/slett/", instrument_delete, name="InstrumentDelete"),
]
