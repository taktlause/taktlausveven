import pghistory
from autoslug import AutoSlugField
from autoslug.settings import slugify
from common.models import CreatedModifiedMixin
from django.conf import settings
from django.db.models import (
    CASCADE,
    RESTRICT,
    SET_NULL,
    BooleanField,
    CharField,
    FloatField,
    ForeignKey,
    Manager,
    Model,
    TextChoices,
    TextField,
    UniqueConstraint,
)


class InstrumentGroup(Model):
    name = CharField(max_length=255, verbose_name="namn", unique=True)
    slug = AutoSlugField(
        verbose_name="lenkjenamn",
        populate_from="name",
        unique=True,
        editable=True,
    )
    order = FloatField(
        "rekkjefølgje",
        default=0,
        help_text="Definerer rekkjefølgja til oppføringar. Oppføringar med lik rekkjefølgje vert sortert etter nykel.",
    )
    is_actual_instrument = BooleanField(
        "Er faktisk instrument",
        default=True,
        help_text="Om gruppa er ein faktisk instrumentgruppe eller ein tøyse gruppe.",
    )

    class Meta:
        verbose_name = "instrumentgruppe"
        verbose_name_plural = "instrumentgrupper"
        ordering = ["order", "name"]

    def __str__(self):
        return self.name


class InstrumentTypeManager(Manager):
    def sheatless_format(self):
        """Returns instrument types in a `Sheatless`-compatible format."""
        return {
            instrument_type.name: {
                "include": instrument_type.detection_keywords.values_list(
                    "keyword", flat=True
                ),
                "exceptions": instrument_type.detection_exceptions.values_list(
                    "exception", flat=True
                ),
            }
            for instrument_type in super().all()
        }


class InstrumentType(Model):
    name = CharField(max_length=255, verbose_name="namn", unique=True)
    group = ForeignKey(
        InstrumentGroup,
        verbose_name="instrumentgruppe",
        related_name="types",
        on_delete=CASCADE,
    )
    order = FloatField(
        "rekkjefølgje",
        default=0,
        help_text="Definerer rekkjefølgja til oppføringar. Oppføringar med lik rekkjefølgje vert sortert etter nykel.",
    )

    objects = InstrumentTypeManager()

    class Meta:
        verbose_name = "instrumenttype"
        verbose_name_plural = "instrumenttyper"
        ordering = ["order", "name"]

    def __str__(self):
        return self.name

    @classmethod
    def unknown(cls):
        group_unknown, created = InstrumentGroup.objects.get_or_create(name="Ukjend")
        type_unknown, created = InstrumentType.objects.get_or_create(
            name="Ukjend", defaults={"group": group_unknown}
        )
        return type_unknown


class InstrumentTypeDetectionKeyword(Model):
    keyword = CharField("nykelord", max_length=255, unique=True)
    instrument_type = ForeignKey(
        InstrumentType,
        verbose_name="instrumenttype",
        related_name="detection_keywords",
        on_delete=CASCADE,
    )

    class Meta:
        verbose_name = "instrumenttypeattkjenningsnykelord"
        verbose_name_plural = "instrumenttypeattkjenningsnykelord"
        ordering = ["keyword"]

    def __str__(self):
        return self.keyword


class InstrumentTypeDetectionException(Model):
    exception = CharField("unntak", max_length=255)
    instrument_type = ForeignKey(
        InstrumentType,
        verbose_name="instrumenttype",
        related_name="detection_exceptions",
        on_delete=CASCADE,
    )

    class Meta:
        verbose_name = "instrumenttypeattkjenningsunntak"
        verbose_name_plural = "instrumenttypeattkjenningsunntak"
        ordering = ["exception"]
        constraints = [
            UniqueConstraint(
                fields=["exception", "instrument_type"],
                name="detection_exception_unique_for_instrument_type",
            )
        ]

    def __str__(self):
        return self.exception


class InstrumentLocation(Model):
    name = CharField(max_length=255, verbose_name="namn", unique=True)

    class Meta:
        verbose_name = "instrumentstad"
        verbose_name_plural = "instrumentstadar"

    def __str__(self):
        return self.name


def generate_instrument_slug(instance: "Instrument"):
    slug = instance.type.name
    if instance.identifier:
        slug += f"-{instance.identifier}"
    return slugify(slug)


@pghistory.track()
class Instrument(CreatedModifiedMixin):
    type = ForeignKey(
        InstrumentType,
        verbose_name="instrumenttype",
        related_name="instruments",
        on_delete=RESTRICT,
    )
    identifier = CharField(max_length=255, verbose_name="identifikator", blank=True)
    slug = AutoSlugField(
        verbose_name="lenkjenamn",
        populate_from=generate_instrument_slug,
        unique=True,
        editable=True,
        always_update=True,
    )
    user = ForeignKey(
        settings.AUTH_USER_MODEL,
        verbose_name="vert lånt av",
        related_name="instruments",
        on_delete=SET_NULL,
        null=True,
        blank=True,
    )
    location = ForeignKey(
        InstrumentLocation,
        verbose_name="stad",
        related_name="instruments",
        on_delete=RESTRICT,
    )
    serial_number = CharField(max_length=255, verbose_name="serienummer", blank=True)
    comment = TextField(verbose_name="kommentar", blank=True)

    class State(TextChoices):
        GOOD = "GOOD", "God"
        OK = "OK", "Ok"
        BAD = "BAD", "Dårleg"
        UNPLAYABLE = "UNPLAYABLE", "Ikkje spelbart"

    state = CharField(
        max_length=255,
        verbose_name="tilstand",
        choices=State.choices,
        default=State.OK,
    )

    class Meta:
        verbose_name = "instrument"
        verbose_name_plural = "instrument"
        ordering = ["type", "identifier"]
        constraints = [
            UniqueConstraint(fields=["type", "identifier"], name="unique_instrument")
        ]

    def __str__(self):
        return f"{self.type} {self.identifier}"

    def get_state_order(self):
        ordering = ["GOOD", "OK", "BAD", "UNPLAYABLE"]
        return ordering.index(self.state)
