from autoslug.settings import slugify
from common.mixins import TestMixin
from django.db import IntegrityError
from django.test import TestCase
from django.urls import reverse

from .factories import (
    InstrumentFactory,
    InstrumentGroupFactory,
    InstrumentLocationFactory,
    InstrumentTypeDetectionExceptionFactory,
    InstrumentTypeDetectionKeywordFactory,
    InstrumentTypeFactory,
)
from .models import (
    InstrumentType,
    InstrumentTypeDetectionException,
    InstrumentTypeDetectionKeyword,
)


class InstrumentGroupTestSuite(TestMixin, TestCase):
    def setUp(self):
        self.instrument_group = InstrumentGroupFactory(name="Tuba")

    def test_to_str(self):
        self.assertEqual(str(self.instrument_group), "Tuba")

    def test_name_unique(self):
        """`name` should be unique."""
        with self.assertRaises(IntegrityError):
            InstrumentGroupFactory(name=self.instrument_group.name)


class InstrumentTypeTestSuite(TestMixin, TestCase):
    def setUp(self):
        self.instrument_type = InstrumentTypeFactory()

    def test_to_str(self):
        """`__str__` should be equal to the instrument type's name."""
        self.assertEqual(str(self.instrument_type), self.instrument_type.name)

    def test_name_unique(self):
        """`name` should be unique."""
        with self.assertRaises(IntegrityError):
            InstrumentTypeFactory(name=self.instrument_type.name)

    def test_unknown_creates_new_instrument_type_if_not_exist(self):
        """Should create a new instrument type if it doesn't already exist."""
        self.assertEqual(InstrumentType.objects.count(), 1)
        InstrumentType.unknown()
        self.assertEqual(InstrumentType.objects.count(), 2)

    def test_unknown_reuses_existing_instrument_type(self):
        """Should re-use existing instrument type if it exists."""
        unknown = InstrumentType.unknown()
        self.assertEqual(InstrumentType.objects.count(), 2)
        self.assertEqual(unknown, InstrumentType.unknown())
        self.assertEqual(InstrumentType.objects.count(), 2)

    def test_should_reuse_existing_instrument_type_even_if_different_group(self):
        """Should re-use the existing instrument type, even if it has a different group."""
        unknown = InstrumentType.unknown()
        self.assertEqual(InstrumentType.objects.count(), 2)
        unknown.instrument_group = InstrumentGroupFactory()
        self.assertEqual(unknown, InstrumentType.unknown())
        self.assertEqual(InstrumentType.objects.count(), 2)


class InstrumentTypeDetectionKeywordTestSuite(TestMixin, TestCase):
    def setUp(self):
        self.detection_keyword = InstrumentTypeDetectionKeywordFactory()

    def test_to_str(self):
        """`__str__` should equal the keyword."""
        self.assertEqual(str(self.detection_keyword), self.detection_keyword.keyword)

    def test_keyword_unique(self):
        """Keywords should be unique."""
        with self.assertRaises(IntegrityError):
            InstrumentTypeDetectionKeywordFactory(
                keyword=self.detection_keyword.keyword
            )

    def test_ordering(self):
        """Should be ordered by the keyword."""
        self.assertModelOrdering(
            InstrumentTypeDetectionKeyword,
            InstrumentTypeDetectionKeywordFactory,
            [
                {"keyword": "a"},
                {"keyword": "b"},
                {"keyword": "c"},
            ],
        )


class InstrumentTypeDetectionExceptionTestSuite(TestMixin, TestCase):
    def setUp(self):
        self.detection_exception = InstrumentTypeDetectionExceptionFactory()

    def test_to_str(self):
        """`__str__` should equal the exception."""
        self.assertEqual(
            str(self.detection_exception), self.detection_exception.exception
        )

    def test_exception_unique_for_each_instrument(self):
        """Exceptions should be unique for each instrument."""
        InstrumentTypeDetectionExceptionFactory(
            exception=self.detection_exception.exception,
            instrument_type=InstrumentTypeFactory(),
        )

        with self.assertRaises(IntegrityError):
            InstrumentTypeDetectionExceptionFactory(
                exception=self.detection_exception.exception,
                instrument_type=self.detection_exception.instrument_type,
            )

    def test_ordering(self):
        """Should be ordered by the exception."""
        self.assertModelOrdering(
            InstrumentTypeDetectionException,
            InstrumentTypeDetectionExceptionFactory,
            [
                {"exception": "a"},
                {"exception": "b"},
                {"exception": "c"},
            ],
        )


class InstrumentLocationTestSuite(TestMixin, TestCase):
    def setUp(self):
        self.instrument_location = InstrumentLocationFactory(name="Lager")

    def test_to_str(self):
        self.assertEqual(str(self.instrument_location), "Lager")

    def test_name_unique(self):
        """`name` should be unique."""
        with self.assertRaises(IntegrityError):
            InstrumentLocationFactory(name=self.instrument_location.name)


class InstrumentTestSuite(TestMixin, TestCase):
    def setUp(self):
        self.instrument = InstrumentFactory()

    def test_to_str(self):
        """`__str__` should include instrument type and identifier."""
        self.assertEqual(
            str(self.instrument), f"{self.instrument.type} {self.instrument.identifier}"
        )

    def test_type_and_identifier_unique_togther(self):
        """Should enforce uniqueness of `type` and `identifier`."""
        with self.assertRaises(IntegrityError):
            InstrumentFactory(
                type=self.instrument.type, identifier=self.instrument.identifier
            )

    def test_slug_includes_instrument_type_name(self):
        """The slug should include the instrument type name."""
        self.assertIn(slugify(self.instrument.type.name), self.instrument.slug)

    def test_slug_includes_instrument_identifier_if_exists(self):
        """The slug should include the instrument identifier, if it exists."""
        instrument_with_identifier = InstrumentFactory(identifier="5")
        self.assertIn(
            slugify(instrument_with_identifier.identifier),
            instrument_with_identifier.slug,
        )


class InstrumentListTestSuite(TestMixin, TestCase):
    def get_url(self):
        return reverse("instruments:InstrumentList")

    def test_requires_login(self):
        self.assertLoginRequired(self.get_url())


class InstrumentCreateTestCase(TestMixin, TestCase):
    def get_url(self):
        return reverse("instruments:InstrumentCreate")

    def test_requires_login(self):
        """Should require login."""
        self.assertLoginRequired(self.get_url())

    def test_requires_permission(self):
        """Should require permission to create instruments."""
        self.assertPermissionRequired(
            self.get_url(),
            "instruments.add_instrument",
        )


class InstrumentUpdateTestCase(TestMixin, TestCase):
    def get_url(self):
        return reverse("instruments:InstrumentUpdate", args=[InstrumentFactory().slug])

    def test_requires_login(self):
        """Should require login."""
        self.assertLoginRequired(self.get_url())

    def test_requires_permission(self):
        """Should require permission to change instruments."""
        self.assertPermissionRequired(
            self.get_url(),
            "instruments.change_instrument",
        )


class InstrumentDeleteTestCase(TestMixin, TestCase):
    def get_url(self):
        return reverse("instruments:InstrumentDelete", args=[InstrumentFactory().slug])

    def test_requires_login(self):
        """Should require login."""
        self.assertLoginRequired(self.get_url())

    def test_requires_permission(self):
        """Should require permission to delete instruments."""
        self.assertPermissionRequired(
            self.get_url(),
            "instruments.delete_instrument",
        )
