from django.apps import AppConfig
from watson import search


class InstrumentsConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "instruments"
    verbose_name = "instrument"

    def ready(self):
        search.register(
            self.get_model("Instrument"),
            fields=(
                "type__name",
                "identifier",
                "user__name",
                "location__name",
                "serial_number",
                "comment",
            ),
        )
