import functools

from django.http import HttpRequest, HttpResponse
from render_block import render_block_to_string


def is_htmx(request: HttpRequest):
    return request.headers.get("Hx-Request", False)


def htmx(view):
    @functools.wraps(view)
    def wrapper(request, *args, **kwargs):
        response = view(request, *args, **kwargs)
        if not is_htmx(request):
            return response

        block_to_use = request.headers.get("use-block")
        return HttpResponse(
            content=render_block_to_string(
                response.template_name,
                block_to_use,
                context=response.context_data,
                request=request,
            ),
            status=response.status_code,
            headers=response.headers,
        )

    return wrapper
