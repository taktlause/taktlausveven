import os

from django.forms import ValidationError
from django.utils.deconstruct import deconstructible


@deconstructible
class FileTypeValidator:
    def __init__(self, allowed_extensions):
        """`allowed_extensions` should be a list of allowed file extensions."""
        self.allowed_extensions = allowed_extensions

    def validate_file(self, file):
        extension = os.path.splitext(file.name)[1].lower()
        if extension not in self.allowed_extensions:
            raise ValidationError(f"{file.name}: Filending '{extension}' ikkje lovleg.")

    def __call__(self, data):
        if isinstance(data, list | tuple):
            for file in data:
                self.validate_file(file)
        else:
            self.validate_file(data)

    def __eq__(self, other):
        return (
            isinstance(other, FileTypeValidator)
            and self.allowed_extensions == other.allowed_extensions
        )
