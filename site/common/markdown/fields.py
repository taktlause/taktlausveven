from django import forms
from django.db import models


class MarkdownFormField(forms.CharField):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        if self.help_text:
            self.help_text += "<br />"
        self.help_text += (
            "Dette feltet brukar Markdown. "
            "Det betyr at teksta du skriv vert formatert som Markdown. "
            "For meir informasjon, sjå "
            '<a href="https://taktlaus.no/wiki/markdown/" target="_blank">taktlaus.no/wiki/markdown/</a>.'
        )


class MarkdownField(models.TextField):
    def formfield(self, **kwargs):
        defaults = {"form_class": MarkdownFormField}
        defaults.update(kwargs)
        return super().formfield(**defaults)
