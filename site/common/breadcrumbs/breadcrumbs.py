"""
breadcrumbs.py

Main components of the breadcrumbs system.

Documentation for the breadcrumbs system can be found at https://gitlab.com/taktlause/taktlausveven/-/wikis/Breadcrumbs
"""

from collections.abc import Callable
from dataclasses import dataclass

from django.views.generic import View


@dataclass(eq=True, frozen=True)
class Breadcrumb:
    """
    Standardized breadcrumb designed to work with `BreadcrumbsMixin` and `common/breadcrumbs/breadcrumbs.html`.

    `str` `url`: The URL the breadcrumb should redirect to
    `str` `label`: The breadcrumb's label
    """

    url: str
    label: str


class BreadcrumbsMixin(View):
    breadcrumb_parent: View = None

    @classmethod
    def get_breadcrumb_parent(cls, **kwargs) -> View:
        return cls.breadcrumb_parent

    @classmethod
    def get_breadcrumb(cls, **kwargs) -> Breadcrumb:
        """Must be overriden to return a `Breadcrumb`."""
        raise NotImplementedError(
            "BreadcrumbsMixin.get_breadcrumb() must be overridden"
        )

    @classmethod
    def get_breadcrumbs_from_parent(cls, **kwargs) -> list:
        parent = cls.get_breadcrumb_parent(**kwargs)
        if parent is None:
            return []
        return parent.get_breadcrumbs_for_children(**kwargs)

    @classmethod
    def get_breadcrumbs_for_children(cls, **kwargs) -> list:
        return [
            *cls.get_breadcrumbs_from_parent(**kwargs),
            cls.get_breadcrumb(**kwargs),
        ]

    def get_breadcrumbs_kwargs(self) -> dict:
        """
        Returns kwargs used by breadcrumb methods of both the current view and all parent views.

        Providing kwargs for the current view's `get_breadcrumb()` is unnecessary,
        since a view should only display its parents' breadcrumbs. A page doesn't need to link to itself.
        """
        return {}

    def get_breadcrumbs(self) -> list:
        return self.get_breadcrumbs_from_parent(**self.get_breadcrumbs_kwargs())

    def get_context_data(self, **kwargs):
        kwargs["breadcrumbs"] = self.get_breadcrumbs()
        return super().get_context_data(**kwargs)


@dataclass
class BreadcrumbGenerator:
    label: str | Callable
    url: str | Callable
    parent_view: Callable | None

    def _get_label(self, **kwargs) -> str:
        if callable(self.label):
            return self.label(**kwargs)
        return self.label

    def _get_url(self, **kwargs) -> str:
        if callable(self.url):
            return self.url(**kwargs)
        return self.url

    def _get_parent(self):
        if self.parent_view is None:
            return None
        return self.parent_view.breadcrumb_generator

    def _breadcrumbs_from_parent_view(self, **kwargs) -> list[Breadcrumb]:
        parent = self._get_parent()
        if parent is None:
            return []
        return parent.breadcrumbs(**kwargs)

    def _breadcrumb(self, **kwargs) -> Breadcrumb:
        return Breadcrumb(self._get_url(**kwargs), self._get_label(**kwargs))

    def breadcrumbs(self, **kwargs) -> list[Breadcrumb]:
        return [
            *self._breadcrumbs_from_parent_view(**kwargs),
            self._breadcrumb(**kwargs),
        ]


def breadcrumbs(parent_view: Callable, **kwargs):
    if not hasattr(parent_view, "breadcrumb_generator"):
        raise ValueError(
            f"The provided `parent_view` {parent_view} does not have a breadcrumb. "
            "Views provided to this function must be decorated "
            "with the `@breadcrumb` decorator."
        )

    return {"breadcrumbs": parent_view.breadcrumb_generator.breadcrumbs(**kwargs)}


def breadcrumb(
    label: str | Callable, url: str | Callable, parent_view: Callable | None = None
):
    if parent_view and not hasattr(parent_view, "breadcrumb_generator"):
        raise ValueError(
            f"The provided `parent_view` {parent_view} does not have a breadcrumb. "
            "Views provided to this decorator must themselves "
            "be decorated with the `@breadcrumb` decorator."
        )

    def decorator(view_func):
        view_func.breadcrumb_generator = BreadcrumbGenerator(label, url, parent_view)
        return view_func

    return decorator
