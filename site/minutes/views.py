from authentication.decorators import permission_or_created
from common.breadcrumbs.breadcrumbs import (
    breadcrumb,
    breadcrumbs,
)
from common.forms.views import delete_view, simple_create_view, simple_update_view
from django.contrib.auth.decorators import login_required, permission_required
from django.shortcuts import get_object_or_404
from django.template.response import TemplateResponse
from django.urls import reverse, reverse_lazy

from .forms import MinutesCategoryForm, MinutesForm
from .models import Minutes, MinutesCategory


@login_required
@breadcrumb("Alle referat", reverse_lazy("minutes:MinutesList"))
def minutes_list(request):
    minutes = Minutes.objects.select_related("created_by", "category")
    return TemplateResponse(
        request,
        "minutes/minutes_list.html",
        {"minutes_list": minutes, "categories": MinutesCategory.objects.all()},
    )


@login_required
@breadcrumb(
    lambda minutes: str(minutes),
    lambda minutes: minutes.get_absolute_url(),
    minutes_list,
)
def minutes_detail(request, slug: str):
    minutes = get_object_or_404(Minutes, slug=slug)
    return TemplateResponse(
        request,
        "minutes/minutes_detail.html",
        {"minutes": minutes} | breadcrumbs(minutes_list),
    )


@login_required
def minutes_create(request):
    return simple_create_view(
        request,
        MinutesForm,
        context=breadcrumbs(minutes_list),
    )


def get_minutes_by_slug(_, slug: str):
    return get_object_or_404(Minutes, slug=slug)


@login_required
@permission_or_created("minutes.change_minutes", object_getter=get_minutes_by_slug)
def minutes_update(request, slug: str):
    minutes = get_object_or_404(Minutes, slug=slug)
    return simple_update_view(
        request,
        MinutesForm,
        minutes,
        context=breadcrumbs(minutes_detail, minutes=minutes),
    )


@login_required
@permission_or_created("minutes.delete_minutes", object_getter=get_minutes_by_slug)
def minutes_delete(request, slug: str):
    minutes = get_object_or_404(Minutes, slug=slug)
    return delete_view(
        request,
        minutes,
        reverse("minutes:MinutesList"),
        context=breadcrumbs(minutes_detail, minutes=minutes),
    )


@login_required
@breadcrumb("Kategoriar", reverse_lazy("minutes:MinutesCategoryList"), minutes_list)
def minutes_category_list(request):
    categories = MinutesCategory.objects.all()
    return TemplateResponse(
        request,
        "minutes/minutes_category_list.html",
        {"categories": categories} | breadcrumbs(minutes_list),
    )


@login_required
@permission_required("minutes.add_minutescategory", raise_exception=True)
def minutes_category_create(request):
    return simple_create_view(
        request,
        MinutesCategoryForm,
        success_url=reverse("minutes:MinutesCategoryList"),
        context=breadcrumbs(minutes_category_list),
    )


@login_required
@permission_required("minutes.change_minutescategory", raise_exception=True)
def minutes_category_update(request, slug: str):
    category = get_object_or_404(MinutesCategory, slug=slug)
    return simple_update_view(
        request,
        MinutesCategoryForm,
        category,
        success_url=reverse("minutes:MinutesCategoryList"),
        context=breadcrumbs(minutes_category_list),
    )


@login_required
@permission_required("minutes.delete_minutescategory", raise_exception=True)
def minutes_category_delete(request, slug: str):
    category = get_object_or_404(MinutesCategory, slug=slug)
    return delete_view(
        request,
        category,
        success_url=reverse_lazy("minutes:MinutesCategoryList"),
        context=breadcrumbs(minutes_category_list),
    )
