from datetime import date

from factory import LazyFunction, sequence
from factory.django import DjangoModelFactory

from .models import Minutes, MinutesCategory


class MinutesCategoryFactory(DjangoModelFactory):
    class Meta:
        model = MinutesCategory

    name = sequence(lambda n: f"Kategory #{n}")


class MinutesFactory(DjangoModelFactory):
    class Meta:
        model = Minutes

    title = "Board Meeting"
    content = "Talk, talk, talk."
    date = LazyFunction(date.today)
