document.addEventListener("DOMContentLoaded", () => {
  let data_table = new DataTable("#minutes-table", {
    order: [[3, "desc"]],
    paging: false,
    language: {
      zeroRecords: "Ingen referat funne",
      info: "Visar _TOTAL_ referat",
      infoEmpty: "Ingen referat funne",
      infoFiltered: "(filtrert fra totalt _MAX_ referat)",
      search: "Søk:",
    },
  });

  const filterTableOnCategory = (category) => {
    data_table
      .column(2)
      .search(category ? category : "", true, false)
      .draw();
  };

  const categorySelecter = document.querySelector("#select-category");

  filterTableOnCategory(categorySelecter.value);
  categorySelecter.addEventListener("input", (event) =>
    filterTableOnCategory(event.target.value)
  );
});
