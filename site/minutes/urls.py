from django.urls import path

from . import views

app_name = "minutes"

urlpatterns = [
    path("", views.minutes_list, name="MinutesList"),
    path("kategoriar/", views.minutes_category_list, name="MinutesCategoryList"),
    path(
        "kategoriar/ny/",
        views.minutes_category_create,
        name="MinutesCategoryCreate",
    ),
    path(
        "kategoriar/<slug:slug>/rediger/",
        views.minutes_category_update,
        name="MinutesCategoryUpdate",
    ),
    path(
        "kategoriar/<slug:slug>/slett/",
        views.minutes_category_delete,
        name="MinutesCategoryDelete",
    ),
    path("ny/", views.minutes_create, name="MinutesCreate"),
    path("<slug:slug>/", views.minutes_detail, name="MinutesDetail"),
    path("<slug:slug>/rediger/", views.minutes_update, name="MinutesUpdate"),
    path("<slug:slug>/slett/", views.minutes_delete, name="MinutesDelete"),
]
