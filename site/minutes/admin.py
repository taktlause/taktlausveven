from django.contrib.admin import ModelAdmin, site

from minutes.models import Minutes, MinutesCategory


class MinutesCategoryAdmin(ModelAdmin):
    list_display = ("name",)


class MinutesAdmin(ModelAdmin):
    list_display = ("title", "created_by", "category", "date")
    search_fields = ("title", "created_by__username", "category__name")
    list_filter = ("category",)

    prepopulated_fields = {"slug": ("title",)}
    list_editable = ("category",)


site.register(MinutesCategory, MinutesCategoryAdmin)
site.register(Minutes, MinutesAdmin)
