from autoslug import AutoSlugField
from common.models import ArticleMixin
from django.db.models import (
    RESTRICT,
    CharField,
    DateField,
    FileField,
    ForeignKey,
    Model,
)
from django.urls import reverse


def default_minutes_category():
    return MinutesCategory.objects.get_or_create(name="Ukjent")[0].pk


class MinutesCategory(Model):
    name = CharField(verbose_name="namn", max_length=255, unique=True)
    slug = AutoSlugField(
        verbose_name="lenkjenamn", populate_from="name", editable=True, unique=True
    )

    class Meta:
        ordering = ["name"]
        verbose_name = "referatkategori"
        verbose_name_plural = "referatkategoriar"

    def __str__(self):
        return self.name


class Minutes(ArticleMixin):
    date = DateField("møtedato", help_text="Datoen møtet vart heldt.")
    file = FileField("fil", upload_to="minutes/", default=None, blank=True)
    slug = AutoSlugField(
        verbose_name="lenkjenamn", populate_from="title", editable=True, unique=True
    )
    category = ForeignKey(
        MinutesCategory,
        verbose_name="kategori",
        related_name="minutes",
        on_delete=RESTRICT,
        default=default_minutes_category,
    )

    def get_absolute_url(self):
        return reverse("minutes:MinutesDetail", args=[self.slug])

    class Meta:
        ordering = ["-date"]
        verbose_name = "referat"
        verbose_name_plural = "referat"
