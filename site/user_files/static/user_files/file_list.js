document.addEventListener("DOMContentLoaded", () => {
  new DataTable("#user-files", {
    order: [[0, "asc"]],
    paging: false,
    language: {
      zeroRecords: "Ingen brukarfiler funne",
      info: "Visar _TOTAL_ brukarfiler",
      infoEmpty: "Ingen brukarfiler funne",
      infoFiltered: "(filtrert fra totalt _MAX_ brukarfiler)",
      search: "Søk:",
    },
    columns: [
      null,
      null,
      null,
      { orderable: false },
      { orderable: false },
      { orderable: false },
    ],
  });
});
