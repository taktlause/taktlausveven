from factory.django import DjangoModelFactory, FileField

from .models import File


class FileFactory(DjangoModelFactory):
    class Meta:
        model = File

    name = "Free World Fantasy"
    file = FileField(filename="Free World Fantasy.mp3")
