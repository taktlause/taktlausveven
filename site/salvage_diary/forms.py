"""Forms for the 'salvage_diary'-app"""
from accounts.models import UserCustom
from common.forms.widgets import (
    AutocompleteSelect,
    AutocompleteSelectMultiple,
    DateDateInput,
)
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit
from django.forms import HiddenInput, ModelForm

from salvage_diary.models import (
    Mascot,
    SalvageDiaryEntryExternal,
    SalvageDiaryEntryInternal,
)


class MascotForm(ModelForm):
    helper = FormHelper()
    helper.add_input(Submit("submit", "Legg inn"))

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["creators"].queryset = UserCustom.objects.exclude(
            membership_status=UserCustom.MembershipStatus.INACTIVE
        )

    class Meta:
        model = Mascot
        fields = [
            "name",
            "image",
            "creation_start_date",
            "creation_end_date",
            "creators",
            "note",
        ]
        widgets = {
            "creators": AutocompleteSelectMultiple,
            "creation_start_date": DateDateInput,
            "creation_end_date": DateDateInput,
        }


class SalvageDiaryEntryExternalForm(ModelForm):
    helper = FormHelper()
    helper.add_input(Submit("submit", "Legg inn"))

    def __init__(self, mascot, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.initial["mascot"] = mascot
        self.fields["mascot"].disabled = True

    class Meta:
        model = SalvageDiaryEntryExternal
        fields = ["title", "thieves", "event", "image", "story", "mascot"]
        widgets = {
            "mascot": HiddenInput,
        }


class SalvageDiaryEntryExternalUpdateForm(ModelForm):
    helper = FormHelper()
    helper.add_input(Submit("submit", "Legg inn"))

    class Meta:
        model = SalvageDiaryEntryExternal
        fields = ["title", "thieves", "event", "image", "story"]


class SalvageDiaryEntryInternalForm(ModelForm):
    helper = FormHelper()
    helper.add_input(Submit("submit", "Legg inn"))

    class Meta:
        model = SalvageDiaryEntryInternal
        fields = ["title", "item", "thieves", "users", "event", "image", "story"]
        widgets = {
            "event": AutocompleteSelect,
            "users": AutocompleteSelectMultiple,
        }
