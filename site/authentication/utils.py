from django.contrib.auth.models import Permission


def find_permission_instance(permission_string: str):
    """
    Returns the `Permission` instance given by `permission_string`,
    where `permission_string` has the format `<app label>.<codename>`.
    Returns None if no permission is found.
    """
    app_label, codename = permission_string.split(".")
    return Permission.objects.filter(
        codename=codename, content_type__app_label=app_label
    ).first()
