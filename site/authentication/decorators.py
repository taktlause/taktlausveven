import functools

from django.core.exceptions import PermissionDenied
from django.http import HttpRequest


def permission_or_created(perm, object_getter):
    def decorator(view_func):
        @functools.wraps(view_func)
        def wrapper(request: HttpRequest, *args, **kwargs):
            perms = (perm,) if isinstance(perm, str) else perm
            obj = object_getter(request, *args, **kwargs)

            if not (request.user.has_perms(perms) or obj.created_by == request.user):
                raise PermissionDenied()

            return view_func(request, *args, **kwargs)

        return wrapper

    return decorator
