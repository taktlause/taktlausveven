from django.contrib import admin

from feedback.models import FeedbackCategory, FeedbackRecipient


class CategoryInline(admin.TabularInline):
    model = FeedbackCategory
    extra = 0


class FeedbackCategoryAdmin(admin.ModelAdmin):
    list_display = ("name", "feedback_recipient")
    search_fields = ("name",)
    list_filter = ("feedback_recipient",)


class FeedbackRecipientAdmin(admin.ModelAdmin):
    list_display = ("name", "is_visible")
    search_fields = ("name",)
    inlines = (CategoryInline,)


admin.site.register(FeedbackCategory, FeedbackCategoryAdmin)
admin.site.register(FeedbackRecipient, FeedbackRecipientAdmin)
