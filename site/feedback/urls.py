from django.urls import path

from . import views

app_name = "feedback"

urlpatterns = [
    # Dynamic url corresponding to the current FeedbackRecipient model
    path("<slug:slug>/", views.FeedbackView.as_view(), name="FeedbackView"),
]
