from crispy_forms.helper import FormHelper
from crispy_forms.layout import Field, Layout, Submit
from django.forms import CharField, Form, ModelChoiceField, Textarea

from feedback.models import FeedbackCategory


class FeedbackForm(Form):
    """Form for sending feedback via the websita"""

    def __init__(self, recipient=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.recipient = recipient

        # Sets the 'category' options to the categories relating to the current FeedbackRecipient
        self.fields["category"].queryset = FeedbackCategory.objects.filter(
            feedback_recipient=recipient
        )

    def get_recipient(self):
        # Used in the view for accessing recipient corresponding to the current form
        return self.recipient

    category = ModelChoiceField(
        label="Kategori",
        to_field_name="name",
        queryset=FeedbackCategory.objects.none(),
        empty_label=None,
        required=True,
    )
    message = CharField(label="Melding", max_length=500, widget=Textarea)

    helper = FormHelper()
    helper.field_class = "col-lg-8"
    helper.layout = Layout(
        Field("category"),
        Field("message"),
        Submit("submit", "Send melding"),
    )
