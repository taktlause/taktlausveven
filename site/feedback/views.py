import logging
from smtplib import SMTPException

import requests
from django.conf import settings
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.mail import EmailMessage
from django.shortcuts import render
from django.views.generic import FormView

from feedback.forms import FeedbackForm
from feedback.models import FeedbackRecipient

logger = logging.getLogger(__name__)


class FeedbackView(LoginRequiredMixin, FormView):
    template_name = "feedback/feedback_form.html"
    template_success_name = "feedback/feedback_success.html"
    form_class = FeedbackForm

    """
    get_initial, get_context_data and get_form_kwargs are used to access the data for the 
    corresponding FeedbackRecipient for usage in the form
    """

    def get_context_data(self, **kwargs):
        # Put the data from the associated model instance into the context
        context = super().get_context_data(**kwargs)

        recipient = FeedbackRecipient.objects.get(slug=self.kwargs["slug"])

        # Used for displaying the correct info in the html
        context["recipient_name"] = recipient.name
        context["recipient_text"] = recipient.form_text

        return context

    def get_form_kwargs(self, **kwargs):
        # Send the recipient name to the Field
        kwargs = super().get_form_kwargs(**kwargs)
        kwargs["recipient"] = FeedbackRecipient.objects.get(slug=self.kwargs["slug"])

        return kwargs

    def form_valid(self, form):
        recipient = form.get_recipient()
        context = {"feedback_response": "Feedback failed"}
        match recipient.message_method:
            case "discord":
                # Send message on discord
                context = send_discord_feedback(form, recipient)

            case "email":
                try:
                    context = send_mail_feedback(form, recipient)
                except SMTPException:
                    form.add_error(
                        None, "Sendinga av meldinga mislykkast. Prøv igjen seinare."
                    )
                    return self.form_invalid(form)

        return render(self.request, self.template_success_name, context=context)


def send_discord_feedback(form, recipient):
    # Send message on discord
    message = {
        "content": f""" Sendt via tilbakemeldingsskjemaet på veven.
Kategori: {form.cleaned_data['category']} \n``` {form.cleaned_data['message']}``` """,
    }

    requests.post(recipient.message_url, json=message)
    return {"feedback_response": recipient.success_text}


def send_mail_feedback(form, recipient):
    email_subject = f"[Tilbakemelding] {form.cleaned_data['category'].name}"
    email_body = f"Sendt via tilbakemeldingsskjemaet på veven: \n\n{form.cleaned_data['message']}"

    EmailMessage(
        email_subject,
        email_body,
        settings.EMAIL_HOST_USER,
        [form.get_recipient().message_url],
    ).send(fail_silently=False)
    return {"feedback_response": recipient.success_text}
