import factory

from .models import FeedbackCategory, FeedbackRecipient


class FeedbackRecipientFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = FeedbackRecipient

    name = factory.sequence(lambda n: f"FeedbackRecipient #{n}")
    message_url = factory.sequence(lambda x: f"Random link #{x}")


class FeedbackCategoryFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = FeedbackCategory

    name = factory.sequence(lambda n: f"FeedbackCategory #{n}")
    feedback_recipient = factory.SubFactory(FeedbackRecipientFactory)
