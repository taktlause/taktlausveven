# Create your models here.
from autoslug import AutoSlugField
from django.db import models
from django.db.models import (
    BooleanField,
    CharField,
    ForeignKey,
    Model,
    TextField,
    UniqueConstraint,
)


class FeedbackRecipient(Model):
    """
    Model containing the information related to the targets of the feedback
    """

    name = CharField("mottakarnamn", max_length=63, unique=True)
    slug = AutoSlugField(
        verbose_name="lenkjenamn",
        populate_from="name",
        editable=True,
        unique=True,
    )

    class MessageMethods(models.TextChoices):
        DISCORD = "discord", "Discord"
        EMAIL = "email", "Email"

    message_method = CharField(
        "meldingsmetode",
        max_length=63,
        choices=MessageMethods.choices,
        help_text="Metoden for å gje tilbakemeldinga",
    )

    message_url = CharField(
        "meldingsurl",
        max_length=255,
        help_text="Identifikatoren for den korrekte mottakaren. En email-adresse for email, og en webook-url for discord",
    )

    form_text = TextField(
        "skjematekst",
        max_length=1000,
        help_text="Tekst som forklarar innhaldet til skjemaet",
    )

    success_text = TextField(
        "suksesstekst",
        max_length=1000,
        help_text="Tekst responsen for suksessfull tilbakemelding",
    )

    is_visible = BooleanField(
        "synlegheit",
        default=True,
        help_text="Styrer om linken er synleg i side-panelet",
    )

    class Meta:
        ordering = ["name"]
        verbose_name = "Tilbakemeldingmottakar"
        verbose_name_plural = "Tilbakemeldingmottakarar"

    def __str__(self):
        return self.name


class FeedbackCategory(Model):
    """
    Model for specifying categories on the contact form.

    `name` is the name of the category.
    'feedback_recipient' is for specifying who is receiving the feedback
    """

    feedback_recipient = ForeignKey(
        FeedbackRecipient,
        verbose_name="tilbakemeldingsmottakar",
        on_delete=models.PROTECT,
        help_text="Tilbakemeldingsmottakaren kategorien tilhøyrer",
    )
    name = CharField(
        "namn",
        max_length=255,
        blank=False,
    )

    class Meta:
        constraints = [
            UniqueConstraint(
                "name",
                "feedback_recipient",
                name="unique_name_recipient",
                violation_error_message="Kategorien fins allereie for den valgte mottakeren.",
            )
        ]
        ordering = ["name"]
        verbose_name = "tilbakemeldingkategori"
        verbose_name_plural = "tilbakemeldingkategoriar"

    def __str__(self):
        return self.name
