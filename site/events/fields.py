from django.forms import ModelMultipleChoiceField
from django.template.defaultfilters import date

from .models import Event


class EventMultipleChoiceField(ModelMultipleChoiceField):
    def label_from_instance(self, obj: Event) -> str:
        return f"{obj} ({date(obj.start_time, 'Y-m-d')})"
