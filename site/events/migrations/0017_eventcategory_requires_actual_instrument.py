# Generated by Django 4.2.2 on 2024-10-10 19:08

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("events", "0016_alter_event_created_by_alter_event_modified_by"),
    ]

    operations = [
        migrations.AddField(
            model_name="eventcategory",
            name="requires_actual_instrument",
            field=models.BooleanField(
                default=False,
                help_text="Om dropdownen for instrumenttype skal kun ha 'skikkelige' instrumentgrupper.",
                verbose_name="krever faktisk instrument",
            ),
        ),
    ]
