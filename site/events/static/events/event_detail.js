document.addEventListener("DOMContentLoaded", () => {
  document.querySelectorAll(".table-attendance").forEach(
    (table) =>
      new DataTable(table, {
        paging: false,
        searching: false,
        info: false,
        order: [],
        // Needed because of conflict with Bootstrap's `table-responsive`
        bAutoWidth: false,
        columns: [
          { orderable: false },
          null,
          //Order based on [instrument_group_order, instrument_group, instrument_type_order, instrument_type]
          {orderData: [5,4,3,2]},
          {visible: false},
          {visible: false},
          {visible: false},
          null,
          { orderable: false },
          { orderable: false },
        ],
      })
  );
});
