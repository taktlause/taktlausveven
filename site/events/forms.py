from common.forms.layouts import DynamicFormsetButton, FormsetLayoutObject
from common.forms.widgets import (
    AutocompleteSelect,
    AutocompleteSelectMultiple,
    SplitDateTimeWidgetCustom,
)
from crispy_forms.helper import FormHelper
from crispy_forms.layout import HTML, Fieldset, Layout, Submit
from django.forms import (
    ModelForm,
    NumberInput,
    SplitDateTimeField,
    TextInput,
    inlineformset_factory,
)
from django.urls import reverse
from django.utils.timezone import localtime, now
from instruments.models import InstrumentType
from pictures.fields import GalleryMultipleChoiceField
from sheetmusic.fields import ScoreMultipleChoiceField

from .models import Event, EventAttendance, EventKeyinfoEntry


class EventForm(ModelForm):
    """Form for creating and editing events."""

    start_time = SplitDateTimeField(
        label="Starttid", widget=SplitDateTimeWidgetCustom(), initial=now
    )
    end_time = SplitDateTimeField(
        label="Sluttid", widget=SplitDateTimeWidgetCustom(), required=False
    )

    helper = FormHelper()
    helper.add_input(Submit("submit", "Lagre hending"))
    helper.layout = Layout(
        "title",
        "category",
        "start_time",
        "end_time",
        "location",
        "location_map_link",
        Fieldset(
            "Nykelinfo",
            FormsetLayoutObject(),
        ),
        "content",
        Fieldset(
            "Repertoar",
            "include_active_repertoires",
            "repertoires",
            "extra_scores",
        ),
        "galleries",
    )

    class Meta:
        model = Event
        fields = [
            "title",
            "category",
            "start_time",
            "end_time",
            "content",
            "location",
            "location_map_link",
            "include_active_repertoires",
            "repertoires",
            "extra_scores",
            "galleries",
        ]
        field_classes = {
            "extra_scores": ScoreMultipleChoiceField,
            "galleries": GalleryMultipleChoiceField,
        }
        widgets = {
            "category": AutocompleteSelect,
            "repertoires": AutocompleteSelectMultiple,
            "extra_scores": AutocompleteSelectMultiple,
            "galleries": AutocompleteSelectMultiple,
        }


class EventAttendanceForm(ModelForm):
    """Form for registering event attendance."""

    def __init__(self, *args, event=None, htmx=False, **kwargs):
        super().__init__(*args, **kwargs)

        attendance = kwargs.get("instance")

        self.fields["instrument_type"].queryset = (
            InstrumentType.objects.filter(group__is_actual_instrument=True)
            if event.category.requires_actual_instrument
            else InstrumentType.objects.all()
        )

        self.helper = FormHelper()
        self.helper.field_class = "col-lg-4"
        self.helper.layout = Layout(
            HTML(
                "<dl class='dl-inline'>"
                "<dt>Ditt svar</dt>"
                f"<dd>{attendance.current_registration() if attendance else 'Ikkje svart'}</dd>"
                "</dl>"
            ),
            "status",
            "instrument_type",
            Submit("submit", "Oppdater svar" if attendance else "Send svar"),
        )

        if not attendance:
            self.helper.form_action = reverse(
                "events:EventAttendanceCreate",
                args=[
                    localtime(event.start_time).year,
                    event.slug,
                ],
            )
        else:
            self.helper.form_action = reverse(
                "events:EventAttendanceUpdate",
                args=[
                    localtime(event.start_time).year,
                    event.slug,
                    attendance.person.slug,
                ],
            )

        if htmx:
            self.helper.attrs = {
                "data-hx-boost": "true",
                "data-hx-target": "closest .card",
                # `show:no-scroll` isn't valid HTMX, but does prevent scrolling on swap.
                # https://www.reddit.com/r/htmx/comments/125gha4/comment/jkf6a2n/?utm_source=share&utm_medium=web3x&utm_name=web3xcss&utm_term=1&utm_content=share_button
                "data-hx-swap": "outerHTML show:no-scroll",
                "data-hx-push-url": "false",
            }

    class Meta:
        model = EventAttendance
        fields = ["status", "instrument_type"]
        widgets = {
            "instrument_type": AutocompleteSelect,
        }


class EventKeyinfoEntryForm(ModelForm):
    """Form for updating a keyinfo entry."""

    class Meta:
        model = EventKeyinfoEntry
        fields = ["key", "info", "order"]
        help_texts = {"order": ""}
        widgets = {
            "key": TextInput(attrs={"size": 8}),
            "order": NumberInput(attrs={"size": 4}),
        }


EventKeyinfoEntryFormset = inlineformset_factory(
    Event,
    EventKeyinfoEntry,
    form=EventKeyinfoEntryForm,
    extra=1,
)


class EventKeyinfoEntryFormsetHelper(FormHelper):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.add_input(DynamicFormsetButton("Legg til endå ein nykelinfo"))
        self.template = "common/forms/table_inline_formset_shade_delete.html"


EventKeyinfoEntryFormset.helper = EventKeyinfoEntryFormsetHelper()
