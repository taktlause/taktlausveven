import factory

from .models import Forum, Topic


class ForumFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Forum

    title = "General"
    description = "For general stuff."


class TopicFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Topic

    title = "Read only?"
    forum = factory.SubFactory(ForumFactory)
