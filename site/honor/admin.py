from django.contrib.admin import ModelAdmin, site

from .models import Honor


class HonorAdmin(ModelAdmin):
    list_display = ("user", "appointment_date", "event_awarded_at")
    search_fields = ("user__name", "appointment_date", "event_awarded_at__title")

    readonly_fields = ("created", "created_by", "modified", "modified_by")


site.register(Honor, HonorAdmin)
