from common.breadcrumbs.breadcrumbs import Breadcrumb, BreadcrumbsMixin
from common.forms.views import DeleteViewCustom
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.db.models import Q
from django.http import Http404
from django.urls import reverse, reverse_lazy
from django.views.generic import CreateView, ListView, UpdateView

from .forms import HonorCreateForm, HonorUpdateForm
from .models import Honor


class HonorList(LoginRequiredMixin, BreadcrumbsMixin, ListView):
    model = Honor
    context_object_name = "honors"
    template_name = "honor/honor_list.html"

    def get_filter(self) -> Q:
        match self.kwargs.get("filter_type"):
            case "hedersmedlem":
                return Q(honor_type=Honor.HonorType.COMMEMORATIVE)
            case "æresmedlem":
                return Q(honor_type=Honor.HonorType.HONORARY)
            case None:
                return Q()
            case _:
                raise Http404(f"Ugyldig filtertype: {self.kwargs['filter_type']}")

    def get_queryset(self):
        return Honor.objects.filter(self.get_filter())

    @classmethod
    def get_breadcrumb(cls, **kwargs):
        return Breadcrumb(reverse("honor:HonorList"), "Heder- og æresmedlemmar")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        match self.kwargs.get("filter_type"):
            case "hedersmedlem":
                context["title"] = "Hedersmedlemmar"
                context["embeddable_text"] = "Hedersmedlemmar"
            case "æresmedlem":
                context["title"] = "Æresmedlemmar"
                context["embeddable_text"] = "Æresmedlemmar"
            case _:
                context["title"] = "Heder- og æresmedlemmar"
                context["embeddable_text"] = "Heder"
        return context


class HonorCreate(
    PermissionRequiredMixin,
    SuccessMessageMixin,
    BreadcrumbsMixin,
    CreateView,
):
    model = Honor
    form_class = HonorCreateForm
    template_name = "common/forms/form.html"
    success_message = "Heder laga for %(name)s."
    permission_required = "honor:add_honor"
    breadcrumb_parent = HonorList
    success_url = reverse_lazy("honor:HonorList")

    def get_success_message(self, cleaned_data):
        name = self.object.name if not self.object.user else self.object.user

        return self.success_message % dict(
            cleaned_data,
            name=name,
        )


class HonorUpdate(PermissionRequiredMixin, BreadcrumbsMixin, UpdateView):
    model = Honor
    form_class = HonorUpdateForm
    template_name = "common/forms/form.html"
    breadcrumb_parent = HonorList
    success_url = reverse_lazy("honor:HonorList")
    permission_required = "honor:change_honormember"


class HonorDelete(PermissionRequiredMixin, BreadcrumbsMixin, DeleteViewCustom):
    model = Honor
    breadcrumb_parent = HonorList
    success_url = reverse_lazy("honor:HonorList")
    permission_required = "honor:delete_honormember"
