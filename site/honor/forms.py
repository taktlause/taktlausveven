from accounts.models import UserCustom
from common.forms.widgets import AutocompleteSelect, DateDateInput
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit
from django.forms import ModelForm

from .models import Honor


class HonorCreateForm(ModelForm):
    helper = FormHelper()
    helper.add_input(Submit("submit", "Lagre heder"))

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["user"].queryset = UserCustom.objects.exclude(
            membership_status=UserCustom.MembershipStatus.INACTIVE
        )

    class Meta:
        model = Honor
        fields = [
            "user",
            "name",
            "image",
            "nomination_text",
            "appointment_date",
            "event_awarded_at",
            "honor_type",
        ]
        widgets = {
            "user": AutocompleteSelect,
            "appointment_date": DateDateInput,
            "event_awarded_at": AutocompleteSelect,
            "honor_type": AutocompleteSelect,
        }


class HonorUpdateForm(ModelForm):
    helper = FormHelper()
    helper.add_input(Submit("submit", "Lagre heder"))

    class Meta:
        model = Honor
        fields = ["image", "nomination_text", "appointment_date", "event_awarded_at"]
        widgets = {
            "appointment_date": DateDateInput,
            "event_awarded_at": AutocompleteSelect,
        }
