from autoslug import AutoSlugField
from common.markdown.fields import MarkdownField
from common.models import CreatedModifiedMixin
from django.conf import settings
from django.db.models import (
    CASCADE,
    CharField,
    CheckConstraint,
    DateField,
    F,
    ForeignKey,
    ImageField,
    Q,
    TextChoices,
    UniqueConstraint,
)
from django.templatetags.static import static
from django.utils.text import slugify
from events.models import Event


def generate_honor_slug(instance: "Honor"):
    if instance.user is None:
        return slugify(f"{instance.name}-{instance.honor_type}")
    return slugify(f"{instance.user.username}-{instance.honor_type}")


class Honor(CreatedModifiedMixin):
    user = ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=CASCADE,
        verbose_name="brukar",
        related_name="honors",
        blank=True,
        null=True,
    )
    name = CharField(
        "navn",
        max_length=255,
        blank=True,
        help_text="Dersom medlemmen ikkje har brukar.",
    )
    image = ImageField("bilete", upload_to="honor/", blank=True)
    nomination_text = MarkdownField("nominasjonstekst", blank=True)
    appointment_date = DateField(
        "utnemningsdato",
        null=True,
        blank=True,
        help_text="Datoen medlemmen vart utnemnt til hederet. Det vil si datoen medlemmen vart stemt inn, t.d. datoen til det ekstraordinære årsmøtet for æresmedlemmar.",
    )
    event_awarded_at = ForeignKey(
        Event,
        on_delete=CASCADE,
        verbose_name="utdelt på",
        help_text="Hendinga hederet vart utdelt på.",
        related_name="honors",
        blank=True,
        null=True,
    )

    class HonorType(TextChoices):
        COMMEMORATIVE = "COMMEMORATIVE", "Hedersmedlem"
        HONORARY = "HONORARY", "Æresmedlem"

    honor_type = CharField(
        "hedertype",
        max_length=30,
        choices=HonorType.choices,
        default=HonorType.COMMEMORATIVE,
    )
    slug = AutoSlugField(
        verbose_name="lenkjenamn",
        populate_from=generate_honor_slug,
        unique=True,
    )

    def __str__(self):
        if self.user is None:
            return f"{self.name} – {self.get_honor_type_display()}"
        return f"{self.user} – {self.get_honor_type_display()}"

    def default_image(self):
        """
        Returns a URL to the default image.
        """
        match self.honor_type:
            case self.HonorType.COMMEMORATIVE:
                return static("honor/default-commemorative-member.png")
            case self.HonorType.HONORARY:
                return static("honor/default-honorary-member.png")

        return static("accounts/default-avatar.svg")

    class Meta:
        ordering = [F("appointment_date").desc(nulls_last=True)]
        verbose_name = "Heder"
        verbose_name_plural = "Heder"
        constraints = [
            UniqueConstraint(
                "user",
                "honor_type",
                name="honor_only_one_honor_per_user_per_honor_type",
                violation_error_message="Brukaren har allereie hedertypa.",
            ),
            CheckConstraint(
                check=(~Q(user=None) ^ ~Q(name="")),
                name="user_or_name_must_be_set",
                violation_error_message="Brukar eller namn må vere sett, men ikkje begge",
            ),
        ]
