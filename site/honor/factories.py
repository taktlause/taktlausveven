from accounts.factories import UserFactory
from django.utils.timezone import localdate
from factory import LazyFunction, SubFactory
from factory.django import DjangoModelFactory

from .models import Honor


class HonorFactory(DjangoModelFactory):
    class Meta:
        model = Honor

    user = SubFactory(UserFactory)
    nomination_text = "For å vera ein bra medlem."
    appointment_date = LazyFunction(localdate)
    honor_type = Honor.HonorType.COMMEMORATIVE
