"""URLs for the 'salvageDiary'-app"""
from django.urls import path

from .views import HonorCreate, HonorDelete, HonorList, HonorUpdate

app_name = "honor"

urlpatterns = [
    path("", HonorList.as_view(), name="HonorList"),
    path(
        "filter/<str:filter_type>/",
        HonorList.as_view(),
        name="HonorListFilter",
    ),
    path("ny/", HonorCreate.as_view(), name="HonorCreate"),
    path(
        "<slug:slug>/endre/",
        HonorUpdate.as_view(),
        name="HonorUpdate",
    ),
    path(
        "<slug:slug>/slett/",
        HonorDelete.as_view(),
        name="HonorDelete",
    ),
]
