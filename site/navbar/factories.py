from authentication.utils import find_permission_instance
from factory import post_generation, sequence
from factory.django import DjangoModelFactory

from .models import NavbarItem


class NavbarItemFactory(DjangoModelFactory):
    class Meta:
        model = NavbarItem

    text = "Heim"
    order = sequence(lambda n: n)
    type = NavbarItem.Type.LINK
    parent = None

    @post_generation
    def permissions(self, create, permission_strings):
        if not create or not permission_strings:
            return

        self.permissions.set(
            [
                find_permission_instance(permission_string)
                for permission_string in permission_strings
            ]
        )
