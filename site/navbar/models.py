from django.contrib.auth.models import Permission
from django.db.models import (
    SET_NULL,
    BooleanField,
    CharField,
    FloatField,
    ForeignKey,
    Manager,
    ManyToManyField,
    Model,
    Prefetch,
    TextChoices,
)


class NavbarItemManager(Manager):
    def get_queryset(self):
        return (
            super()
            .get_queryset()
            .prefetch_related(
                Prefetch(
                    "permissions",
                    queryset=Permission.objects.select_related("content_type"),
                ),
                Prefetch(
                    "sub_items__permissions",
                    queryset=Permission.objects.select_related("content_type"),
                ),
            )
        )


class NavbarItem(Model):
    text = CharField(verbose_name="tekst", max_length=255)
    link = CharField(verbose_name="lenkjepeikar", max_length=255, blank=True)
    order = FloatField(verbose_name="rekkjefølgje", default=0)
    requires_login = BooleanField(verbose_name="krev innlogging", default=False)

    class Type(TextChoices):
        LINK = "LINK", "Lenkje"
        DROPDOWN = "DROPDOWN", "Nedfallsmeny"

    type = CharField(
        verbose_name="type",
        max_length=255,
        choices=Type.choices,
        default=Type.LINK,
    )
    parent = ForeignKey(
        "self",
        verbose_name="underpunkt av",
        related_name="sub_items",
        blank=True,
        null=True,
        on_delete=SET_NULL,
        limit_choices_to={"type": Type.DROPDOWN},
    )

    permissions = ManyToManyField(Permission, related_name="+")

    objects = NavbarItemManager()

    class Meta:
        verbose_name = "navigasjonslinepunkt"
        verbose_name_plural = "navigasjonslinepunkt"
        ordering = ["order", "text"]

    def __str__(self):
        match self.type:
            case self.Type.LINK:
                return f"{self.text} ({self.link})"
            case _:
                return f"{self.text} ({self.get_type_display()})"

    def active(self, request_path):
        """Returns True if navbar_item is active and False if not."""
        match self.type:
            case NavbarItem.Type.LINK:
                return request_path.startswith(self.link)
            case NavbarItem.Type.DROPDOWN:
                return any(
                    request_path.startswith(sub_item.link)
                    for sub_item in self.sub_items.all()
                )
            case _:
                return False

    def permitted(self, user):
        """
        Returns `True` if `user` is permitted to access this navbar item and `False` if not.
        If the item is a dropdown and `user` does not have permission to any sub-items
        the item is also considered unpermitted for `user`.
        """
        if self.requires_login and not user.is_authenticated:
            return False
        for permission in self.permissions.all():
            permission_string = (
                f"{permission.content_type.app_label}.{permission.codename}"
            )
            if not user.has_perm(permission_string):
                return False
        return self.type == NavbarItem.Type.LINK or any(
            sub_item.permitted(user) for sub_item in self.sub_items.all()
        )
