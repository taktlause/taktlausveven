document.addEventListener("DOMContentLoaded", () => {
  new DataTable("#instrument-group-leader-table", {
    order: [[1, "asc"]],
    paging: false,
    language: {
      zeroRecords: "Ingen instrumentgruppeleiarar funne",
      info: "Visar _TOTAL_ instrumentgruppeleiarar",
      infoEmpty: "Ingen instrumentgruppeleiarar funne",
      infoFiltered: "(filtrert fra totalt _MAX_ instrumentgruppeleiarar)",
      search: "Søk:",
    },
    columns: [{ orderable: false }, null, null],
  });
});
