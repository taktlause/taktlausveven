"""web URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.http import HttpRequest, HttpResponseRedirect
from django.shortcuts import render
from django.templatetags.static import static as static_url
from django.urls import include, path, reverse
from django.views.csrf import csrf_failure as default_csrf_failure
from django.views.generic import RedirectView

urlpatterns = [
    path("admin/", admin.site.urls),
    path("__debug__/", include("debug_toolbar.urls")),
    path(
        "favicon.ico/",
        RedirectView.as_view(url=static_url("images/favicon.ico"), permanent=True),
    ),
    path("brukarar/", include("accounts.urls")),
    path("", include("authentication.urls")),
    path("sitat/", include("quotes.urls")),
    path("lagertilgjenge/", include("storage.urls")),
    path("hendingar/", include("events.urls")),
    path("", include("dashboard.urls")),
    path("notar/", include("sheetmusic.urls")),
    path("kommentarar/", include("common.comments.urls")),
    path("kontakt/", include("contact.urls")),
    path("repertoar/", include("repertoire.urls")),
    path("buttons/", include("buttons.urls")),
    path("julekalender/", include("advent_calendar.urls")),
    path("instrument/", include("instruments.urls")),
    path("uniformer/", include("uniforms.urls")),
    path("forum/", include("forum.urls")),
    path("avstemmingar/", include("polls.urls")),
    path("fotoarkiv/", include("pictures.urls")),
    path("referat/", include("minutes.urls")),
    path("brygging/", include("brewing.urls")),
    path("brukarfiler/", include("user_files.urls")),
    path("pdfar/", include("common.pdfs.urls")),
    path("bergedagbok/", include("salvage_diary.urls")),
    path("tilbakemeldingar/", include("feedback.urls")),
    path("heder/", include("honor.urls")),
    path("", include("search.urls")),
    path("", include("easter_eggs.urls")),
    path("", include("articles.urls")),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


def handler500(request, template_name="500.html"):
    """We have to override this simple view to get context for the 500-template."""
    return render(request, template_name)


def csrf_failure(request: HttpRequest, reason=""):
    """
    Redirect to the expected URL if the user is trying to log in,
    but has already done so. If not, use the default CSRF failure view.

    The redirect circumvents an issue where the user is presented with
    a "403 Forbidden CSRF verification failed" error when trying to
    log in in a browser tab loaded before logging in in another tab.
    Since the user is already authenticated in this case, it should
    be safe to redirect to the URL the user expects to see. All other CSRF failures
    are met with the default CSRF failure view.

    The redirect does not check the provided credentials. This means that
    the user is redirected no matter what they input in the login form.
    This should be fine, since the user is already authenticated
    and no-one has more than one account.

    The issue is mentioned in the Django documentation here:
    https://docs.djangoproject.com/en/5.0/ref/csrf/#why-might-a-user-encounter-a-csrf-validation-failure-after-logging-in
    """
    if (
        request.path == reverse("login")
        and request.method == "POST"
        and request.user.is_authenticated
    ):
        return HttpResponseRedirect(request.POST.get("next") or "/")
    return default_csrf_failure(request, reason)
