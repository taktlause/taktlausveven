from factory import post_generation
from factory.django import DjangoModelFactory

from .models import Repertoire


class RepertoireFactory(DjangoModelFactory):
    class Meta:
        model = Repertoire

    name = "Repertoire"

    @post_generation
    def scores(self, create, score_list):
        if not create or not score_list:
            return

        self.scores.set(score_list)
