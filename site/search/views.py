from articles.models import Article
from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.views.decorators.http import require_GET
from events.models import Event
from minutes.models import Minutes
from pictures.models import Gallery
from polls.models import Poll
from sheetmusic.models import Score
from watson.search import search

from .forms import SearchForm


@require_GET
@login_required
def search_view(request):
    template = (
        "search/partials/search_results.html"
        if request.headers.get("Hx-Request")
        else "search/search.html"
    )

    search_results = search(
        request.GET.get("query", ""),
        models=(Score, Gallery, Article, Minutes, Poll, Event),
    )

    return render(
        request,
        template,
        {
            "search_results": search_results[:50],
            "form": SearchForm,
        },
    )
