from django.conf import settings
from django.db import models


class Medal(models.Model):
    class MedalType(models.TextChoices):
        year = "year", "årsmedalje"
        board = "board", "styremedalje"
        honor = "honor", "æresmedalje"

    users = models.ManyToManyField(
        verbose_name="brukarar",
        to=settings.AUTH_USER_MODEL,
        blank=True,
    )
    medal_type = models.CharField(
        "Medaljetype", max_length=255, unique=True, choices=MedalType.choices
    )

    class Meta:
        verbose_name = "medalje"
        verbose_name_plural = "medaljar"

    def __str__(self):
        return self.get_medal_type_display()
