from django.contrib.admin import ModelAdmin, site

from .models import Medal


class MedalAdmin(ModelAdmin):
    filter_horizontal = ("users",)


site.register(Medal, MedalAdmin)
