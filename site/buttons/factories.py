from factory import sequence
from factory.django import DjangoModelFactory, ImageField

from .models import ButtonDesign


class ButtonDesignFactory(DjangoModelFactory):
    class Meta:
        model = ButtonDesign

    name = sequence(lambda n: f"Button #{n}")
    image = ImageField(color="red")
