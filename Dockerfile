# syntax=docker/dockerfile:1
FROM node:21-alpine as npm
WORKDIR /app
RUN npm install bootstrap@5.0.1 bootswatch@5.1.3 tom-select@2.1.0

FROM python:3.10.0
ENV PYTHONUNBUFFERED=1
ENV DJANGO_SETTINGS_MODULE=web.settings

WORKDIR /app

COPY --from=npm /app/node_modules /app/node_modules

# Setup sheetmusic requirements first because it takes a long time and rarely changes.
# It's therefore important to be cached by Docker.
COPY scripts/sheetmusic.sh /app/
ARG SHEETMUSIC="yes"
RUN if [ $SHEETMUSIC = yes ]; then sh sheetmusic.sh; fi

# Download dependencies
COPY scripts/download_pdf_js.sh /app/
RUN ./download_pdf_js.sh

ADD https://github.com/sass/dart-sass/releases/download/1.69.5/dart-sass-1.69.5-linux-x64.tar.gz /opt/
RUN tar -C /opt/ -xzvf /opt/dart-sass-1.69.5-linux-x64.tar.gz

COPY site/requirements.txt /app/
RUN pip install --upgrade pip && pip install --no-cache -r requirements.txt
