# Taktlausveven

Studentorchesteret Dei Taktlause's website.

## Development

### Installation

Requires [Docker](https://docs.docker.com/get-docker/) and [docker compose](https://docs.docker.com/compose/install/).

- Clone the repository.
- Build containers and initialize the database with `sh scripts/reset.sh`.

### Running

- Run the project with `sh scripts/up.sh`. The site is accessible at [localhost:8000](http://localhost:8000/). Stop it with `Ctrl+C`.
- Log in as a superuser with the username "leiar" and the password "password". The users "aspirant", "medlem", and "pensjonist" also exist, with the password "password". 

### Building

- When you make changes to any dependencies for [`Dockerfile`](./Dockerfile), rebuild containers with `sh scripts/build.sh`

### Running other commands

- Run a single Django command in the Django Docker container with `sh scripts/run.sh site/manage.py <command>`
- Run a single shell command in the Django Docker container with `sh scripts/run.sh <command>`
- Run a Bash shell in the Django Docker container with `sh scripts/run.sh`

### Cleanup

- Stop and remove Docker containers with `sh scripts/down.sh`
- Delete the database with `sh scripts/delete_db.sh`

### Code Quality

The project uses [Django's test framework](https://docs.djangoproject.com/en/4.0/topics/testing/) for tests and [Ruff](https://docs.astral.sh/ruff/) for formatting and linting.

- Run tests with `sh scripts/test.sh`
- Lint and check formatting with `sh scripts/lint.sh`
  - Fix fixable linter errors and format files with `sh scripts/lint.sh --fix`
- Lint, check formatting, check for missing migrations, and run tests with `sh scripts/verify.sh`
  - Fix fixable linter errors and format files with `sh scripts/verify.sh --fix`

### Overview of handy shell scripts

- `sh scripts/up.sh`: Run the project.
- `sh scripts/down.sh`: Stop and remove all running containers.
- `sh scripts/build.sh`: Rebuild containers.
- `sh scripts/delete_db.sh`: Delete the database.
- `sh scripts/reset.sh`: Delete the database and media files, then initialize new dev data.
- `sh scripts/run.sh`: Run a Bash shell in the Django Docker container.
- `sh scripts/run.sh <command>`: Run `<command>` in the Django Docker container.
- `sh scripts/lint.sh`: Lint and check formatting of all files.
- `sh scripts/lint.sh --fix`: Fix fixable linter errors and format all files.
- `sh scripts/test.sh`: Run all tests.
- `sh scripts/test.sh <app_name>`: Run tests for a single app.
- `sh scripts/verify.sh`: Lint, check formatting, check for missing migrations, and run tests.
- `sh scripts/verify.sh --fix`: Fix fixable linter errors, format files, check for missing migrations, and run tests.
- `sh scripts/startapp.sh <app_name>`: Initialize a new Django app with the name `<app_name>`.
- `sh scripts/migrate.sh`: Make migrations and migrate.
- `sh scripts/load_sql_dump.sh <sql_dump>`: Load a SQL dump of the database.
