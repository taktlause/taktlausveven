#!/bin/sh
set -e               # Exit on error
cd $(dirname $0)/../ # Set working directory to project root

docker compose down -v
docker compose up -d

docker compose exec -T db psql -U taktlaus -d taktlaus_db < $1

docker compose down

sh scripts/migrate.sh
