#!/bin/sh
set -e               # Exit on error
cd $(dirname $0)/../ # Set working directory to project root

usage () {
    echo "Usage:"
    echo "./verify.sh [--fix]"
    echo "--fix:"
    echo "    apply linting changes in addition to running lint/test"
}

if [ $# -gt 1 ]; then
    echo "Too many arguments"
    usage
    exit 1
fi

fix=0

while [ $# -ne 0 ]; do
    case "$1" in
        -h | --help)
            usage
            exit 0
            ;;
        --fix)
            fix=1
            ;;
        *)
            echo "Unknown argument $1"
            usage
            exit 1
            ;;
    esac
    shift
done

if [ $fix -eq 1 ]; then
    sh scripts/lint.sh --fix
else
    sh scripts/lint.sh
fi

if (
    ! docker compose run --rm django \
    site/manage.py makemigrations --check
    ); then
    echo "Model changes without migrations detected."
    echo "This means that there are missing migration files."
    echo "You can create missing migration files by running \`sh scripts/migrate.sh\`."
    exit 1
fi

sh scripts/test.sh
