#!/bin/sh
set -e               # Exit on error
cd $(dirname $0)/../ # Set working directory to project root

usage () {
    echo "Usage:"
    echo "./lint.sh [--fix]"
    echo "--fix:"
    echo "    fix required linting in addition to reporting on it"
}

if [ $# -gt 1 ]; then
    echo "Too many arguments"
    usage
    exit 1
fi

fix=0

while [ $# -ne 0 ]; do
    case "$1" in
        -h | --help)
	    usage
	    exit 0
	    ;;
	--fix)
	    fix=1
            ;;
	*)
            echo "Unknown argument $1"
            usage
	    exit 1
	    ;;
    esac
    shift
done

if [ $fix -eq 1 ]; then
    docker compose run --rm django ruff check . --fix
    docker compose run --rm django ruff format .
else
    docker compose run --rm django ruff check .
    docker compose run --rm django ruff format . --check
fi
