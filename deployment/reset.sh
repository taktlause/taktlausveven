#!/bin/sh
set -e               # Exit on error
cd $(dirname $0)/../ # Set working directory to project root

echo Stopping Docker container and dropping database
docker compose -f deployment/docker-compose.yaml down -v --remove-orphans

echo Rebuilding and running Docker container
docker compose -f deployment/docker-compose.yaml build

echo Migrating database
docker compose -f deployment/docker-compose.yaml run --rm django ./site/manage.py migrate

echo Creating development data
docker compose -f deployment/docker-compose.yaml run --rm django ./site/manage.py create_dev_data

echo Cleaning up Docker container
docker compose -f deployment/docker-compose.yaml down
